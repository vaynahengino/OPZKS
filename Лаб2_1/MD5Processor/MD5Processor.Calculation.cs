﻿using System;
using System.Text;

namespace MD5
{
  public partial class MD5Processor
  {
    private delegate uint Function(uint a, uint b, uint c);

    public uint ExchangeParts(uint word, int count)
    {
      return ((word >> 32 - count) | (word << count));
    }

    private void ExecuteRounds(byte[] array, Tuple<Function, Function, Function, Function> functionsInOrderToExecute)
    {
      uint AA = A;
      uint BB = B;
      uint CC = C;
      uint DD = D;

      Step(functionsInOrderToExecute.Item1, array, A, B, C, D, 0, 7, 1);
      Step(functionsInOrderToExecute.Item1, array, D, A, B, C, 1, 12, 2);
      Step(functionsInOrderToExecute.Item1, array, C, D, A, B, 2, 17, 3);
      Step(functionsInOrderToExecute.Item1, array, B, C, D, A, 3, 22, 4);
      Step(functionsInOrderToExecute.Item1, array, A, B, C, D, 4, 7, 5);
      Step(functionsInOrderToExecute.Item1, array, D, A, B, C, 5, 12, 6);
      Step(functionsInOrderToExecute.Item1, array, C, D, A, B, 6, 17, 7);
      Step(functionsInOrderToExecute.Item1, array, B, C, D, A, 7, 22, 8);
      Step(functionsInOrderToExecute.Item1, array, A, B, C, D, 8, 7, 9);
      Step(functionsInOrderToExecute.Item1, array, D, A, B, C, 9, 12, 10);
      Step(functionsInOrderToExecute.Item1, array, C, D, A, B, 10, 17, 11);
      Step(functionsInOrderToExecute.Item1, array, B, C, D, A, 11, 22, 12);
      Step(functionsInOrderToExecute.Item1, array, A, B, C, D, 12, 7, 13);
      Step(functionsInOrderToExecute.Item1, array, D, A, B, C, 13, 12, 14);
      Step(functionsInOrderToExecute.Item1, array, C, D, A, B, 14, 17, 15);
      Step(functionsInOrderToExecute.Item1, array, B, C, D, A, 15, 22, 16);

      Step(functionsInOrderToExecute.Item2, array, A, B, C, D, 1, 5, 17);
      Step(functionsInOrderToExecute.Item2, array, D, A, B, C, 6, 9, 18);
      Step(functionsInOrderToExecute.Item2, array, C, D, A, B, 11, 14, 19);
      Step(functionsInOrderToExecute.Item2, array, B, C, D, A, 0, 20, 20);
      Step(functionsInOrderToExecute.Item2, array, A, B, C, D, 5, 5, 21);
      Step(functionsInOrderToExecute.Item2, array, D, A, B, C, 10, 9, 22);
      Step(functionsInOrderToExecute.Item2, array, C, D, A, B, 15, 14, 23);
      Step(functionsInOrderToExecute.Item2, array, B, C, D, A, 4, 20, 24);
      Step(functionsInOrderToExecute.Item2, array, A, B, C, D, 9, 5, 25);
      Step(functionsInOrderToExecute.Item2, array, D, A, B, C, 14, 9, 26);
      Step(functionsInOrderToExecute.Item2, array, C, D, A, B, 3, 14, 27);
      Step(functionsInOrderToExecute.Item2, array, B, C, D, A, 8, 20, 28);
      Step(functionsInOrderToExecute.Item2, array, A, B, C, D, 13, 5, 29);
      Step(functionsInOrderToExecute.Item2, array, D, A, B, C, 2, 9, 30);
      Step(functionsInOrderToExecute.Item2, array, C, D, A, B, 7, 14, 31);
      Step(functionsInOrderToExecute.Item2, array, B, C, D, A, 12, 20, 32);

      Step(functionsInOrderToExecute.Item3, array, A, B, C, D, 5, 4, 33);
      Step(functionsInOrderToExecute.Item3, array, D, A, B, C, 8, 11, 34);
      Step(functionsInOrderToExecute.Item3, array, C, D, A, B, 11, 16, 35);
      Step(functionsInOrderToExecute.Item3, array, B, C, D, A, 14, 23, 36);
      Step(functionsInOrderToExecute.Item3, array, A, B, C, D, 1, 4, 37);
      Step(functionsInOrderToExecute.Item3, array, D, A, B, C, 4, 11, 38);
      Step(functionsInOrderToExecute.Item3, array, C, D, A, B, 7, 16, 39);
      Step(functionsInOrderToExecute.Item3, array, B, C, D, A, 10, 23, 40);
      Step(functionsInOrderToExecute.Item3, array, A, B, C, D, 13, 4, 41);
      Step(functionsInOrderToExecute.Item3, array, D, A, B, C, 0, 11, 42);
      Step(functionsInOrderToExecute.Item3, array, C, D, A, B, 3, 16, 43);
      Step(functionsInOrderToExecute.Item3, array, B, C, D, A, 6, 23, 44);
      Step(functionsInOrderToExecute.Item3, array, A, B, C, D, 9, 4, 45);
      Step(functionsInOrderToExecute.Item3, array, D, A, B, C, 12, 11, 46);
      Step(functionsInOrderToExecute.Item3, array, C, D, A, B, 15, 16, 47);
      Step(functionsInOrderToExecute.Item3, array, B, C, D, A, 2, 23, 48);

      Step(functionsInOrderToExecute.Item4, array, A, B, C, D, 0, 6, 49);
      Step(functionsInOrderToExecute.Item4, array, D, A, B, C, 7, 10, 50);
      Step(functionsInOrderToExecute.Item4, array, C, D, A, B, 14, 15, 51);
      Step(functionsInOrderToExecute.Item4, array, B, C, D, A, 5, 21, 52);
      Step(functionsInOrderToExecute.Item4, array, A, B, C, D, 12, 6, 53);
      Step(functionsInOrderToExecute.Item4, array, D, A, B, C, 3, 10, 54);
      Step(functionsInOrderToExecute.Item4, array, C, D, A, B, 10, 15, 55);
      Step(functionsInOrderToExecute.Item4, array, B, C, D, A, 1, 21, 56);
      Step(functionsInOrderToExecute.Item4, array, A, B, C, D, 8, 6, 57);
      Step(functionsInOrderToExecute.Item4, array, D, A, B, C, 15, 10, 58);
      Step(functionsInOrderToExecute.Item4, array, C, D, A, B, 6, 15, 59);
      Step(functionsInOrderToExecute.Item4, array, B, C, D, A, 13, 21, 60);
      Step(functionsInOrderToExecute.Item4, array, A, B, C, D, 4, 6, 61);
      Step(functionsInOrderToExecute.Item4, array, D, A, B, C, 11, 10, 62);
      Step(functionsInOrderToExecute.Item4, array, C, D, A, B, 2, 15, 63);
      Step(functionsInOrderToExecute.Item4, array, B, C, D, A, 9, 21, 64);
      A += AA;
      B += BB;
      C += CC;
      D += DD;
    }

    private void Step(Function f, byte[] array, uint a, uint b, uint c, uint d, uint k, int s, int i)
    {
      uint x = (uint)array[k * 4 + 3] * 16777216 + (uint)array[k * 4 + 2] * 65536 + (uint)array[k * 4 + 1] * 256 + (uint)array[k * 4];
      uint current = a;
      current += f(b, c, d);
      current += x;
      current += T[i - 1];
      current = ExchangeParts(current, s);
      current += b;
      if (a == A) A = current;
      else if (a == B) B = current;
      else if (a == C) C = current;
      else D = current;
    }

    public void InitRegisters()
    {
      A = 0x67452301;
      B = 0xEFCDAB89;
      C = 0x98BADCFE;
      D = 0X10325476;
    }

    private byte[] PrepareArray(byte[] array)
    {
      uint[] mass = new uint[16];
      byte[] res = new byte[array.Length + 1];
      Buffer.BlockCopy(array, 0, res, 0, array.Length);
      res[res.Length - 1] = 0x80;
      while (res.Length % 64 != 56)
      {
        byte[] temp = new byte[res.Length + 1];
        Buffer.BlockCopy(res, 0, temp, 0, res.Length);
        temp[res.Length] = 0;
        res = temp;
      }

      uint Length = (uint)array.Length * 8;
      byte[] tmp = new byte[res.Length + 8];
      Buffer.BlockCopy(res, 0, tmp, 0, res.Length);
      for (int p = 0; p < 8; p++)
      {
        tmp[tmp.Length - 8 + p] = (byte)(Length % Math.Pow(2, 8 * (p + 1)) / Math.Pow(2, 8 * p));
      }
      res = tmp;

      return res;
    }

    public void Calculate(string message, int regime)
    {
      this.Calculate(Encoding.UTF8.GetBytes(message), regime);
    }

    public void Calculate(byte[] message, int regime)
    {
      byte[] preparedMessage = this.PrepareArray(message);

      int count = 0;
      do
      {
        byte[] send = new byte[64];
        Buffer.BlockCopy(preparedMessage, count * 64, send, 0, 64);
        Tuple<Function, Function, Function, Function> quadr = null;

        switch (regime)
        {
          case 0: quadr = new Tuple<Function, Function, Function, Function>(F, G, H, K); break;
          case 1: quadr = new Tuple<Function, Function, Function, Function>(G, H, K, F); break;
          case 2: quadr = new Tuple<Function, Function, Function, Function>(H, K, F, G); break;
          case 3: quadr = new Tuple<Function, Function, Function, Function>(K, F, G, H); break;
        }

        ExecuteRounds(send, quadr);
        count++;
      } while (count * 64 < preparedMessage.Length);
    }
  }
}
