﻿using MD5;
using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Threading;

namespace ca
{
  class Program
  {
    static void Main(string[] args)
    {
      MD5Processor md5 = new MD5Processor();
      int regime = 0;

      for (int u = 0; u < 4; u++)
      {
        if (u == 0)
        {
          Console.WriteLine("_____________________________________________");
          Console.WriteLine();
          Console.WriteLine("Режим F, G, H, K");
          Console.WriteLine();
          regime = 0;
        }
        else if (u == 1)
        {
          Console.WriteLine("_____________________________________________");
          Console.WriteLine();
          Console.WriteLine("Режим G, H, K, F");
          Console.WriteLine();
          regime = 1;
        }
        else if (u == 2)
        {
          Console.WriteLine("_____________________________________________");
          Console.WriteLine();
          Console.WriteLine("Режим H, K, F, G");
          Console.WriteLine();
          regime = 2;
        }
        else
        {
          Console.WriteLine("_____________________________________________");
          Console.WriteLine();
          Console.WriteLine("Режим K, F, G, H");
          Console.WriteLine();
          regime = 3;
        }
        //byte[] toHash = File.ReadAllBytes("Mandriva.7z");

        //FileStream fs = new FileStream("Mandriva.7z", FileMode.Open);
        //long offset = (fs.Length % 64 == 0 ? fs.Length + 64 : fs.Length + fs.Length % 64) - fs.Length;

        byte[] toHash = new byte[64];
        //mmvs.Close();

        Console.WriteLine();
        Console.WriteLine("Получены следующие хэши:");
        {
          byte[] hashes = new byte[16];
          Console.Write($"md5(\"KL.rar\") = ");
          //md5.Calculate(Encoding.ASCII.GetBytes(messages[p]), regime);

          FileStream fs = new FileStream("..\\Debug\\KL.rar", FileMode.Open);
          int offset = (int)fs.Length % 64 == 0 ? 64 : (int)fs.Length % 64;
          fs.Read(toHash, offset, toHash.Length - offset);

          DateTime start = DateTime.Now;
          md5.InitRegisters();
          int res = 0;
          int count = 0;
          do
          {
            md5.Calculate(toHash, regime);
            //Console.WriteLine($"{res} {count}");
            count += res;
          } while ((res = fs.Read(toHash, 0, 64)) > 0);
          TimeSpan timeSpan = DateTime.Now - start;
          fs.Close();

          hashes[0] = (byte)(md5.A % 256);
          hashes[1] = (byte)(md5.A % 65536 / 256);
          hashes[2] = (byte)(md5.A % 16777216 / 65536);
          hashes[3] = (byte)(md5.A / 16777216);
          hashes[4] = (byte)(md5.B % 256);
          hashes[5] = (byte)(md5.B % 65536 / 256);
          hashes[6] = (byte)(md5.B % 16777216 / 65536);
          hashes[7] = (byte)(md5.B / 16777216);
          hashes[8] = (byte)(md5.C % 256);
          hashes[9] = (byte)(md5.C % 65536 / 256);
          hashes[10] = (byte)(md5.C % 16777216 / 65536);
          hashes[11] = (byte)(md5.C / 16777216);
          hashes[12] = (byte)(md5.D % 256);
          hashes[13] = (byte)(md5.D % 65536 / 256);
          hashes[14] = (byte)(md5.D % 16777216 / 65536);
          hashes[15] = (byte)(md5.D / 16777216);
          for (int q = 0; q < 16; q++)
          {
            Console.Write("{0:x2}", hashes[q]);
          }
          Console.WriteLine();
          Console.WriteLine($"Time of hashing: {timeSpan}.");
          Console.WriteLine($"Speed of hashing: {(count / timeSpan.TotalSeconds)/1024/1024} MB/sec.");
          Console.WriteLine();
        }
      }
      Console.ReadKey();
    }
  }
}
