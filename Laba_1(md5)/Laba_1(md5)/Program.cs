﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laba_1_md5_
{
    class Program
    {
        static public uint A = 0x67452301;
        static public uint B = 0xEFCDAB89;
        static public uint C = 0x98BADCFE;
        static public uint D = 0X10325476;

        static public int RoundCount = 1;

        static public int f;
        static public int g;
        static public int h;
        static public int k;

        static public uint[] T = new uint[64]
            {   0xd76aa478,0xe8c7b756,0x242070db,0xc1bdceee,
                0xf57c0faf,0x4787c62a,0xa8304613,0xfd469501,
                0x698098d8,0x8b44f7af,0xffff5bb1,0x895cd7be,
                0x6b901122,0xfd987193,0xa679438e,0x49b40821,
                0xf61e2562,0xc040b340,0x265e5a51,0xe9b6c7aa,
                0xd62f105d,0x2441453,0xd8a1e681,0xe7d3fbc8,
                0x21e1cde6,0xc33707d6,0xf4d50d87,0x455a14ed,
                0xa9e3e905,0xfcefa3f8,0x676f02d9,0x8d2a4c8a,
                0xfffa3942,0x8771f681,0x6d9d6122,0xfde5380c,
                0xa4beea44,0x4bdecfa9,0xf6bb4b60,0xbebfbc70,
                0x289b7ec6,0xeaa127fa,0xd4ef3085,0x4881d05,
                0xd9d4d039,0xe6db99e5,0x1fa27cf8,0xc4ac5665,
                0xf4292244,0x432aff97,0xab9423a7,0xfc93a039,
                0x655b59c3,0x8f0ccc92,0xffeff47d,0x85845dd1,
                0x6fa87e4f,0xfe2ce6e0,0xa3014314,0x4e0811a1,
                0xf7537e82,0xbd3af235,0x2ad7d2bb,0xeb86d391};

        static public uint Rot(uint ish, int count)
        {
            return ((ish >> 32 - count) | (ish << count));
        }

        static public uint F(uint X, uint Y, uint Z)
        {
            return (X & Y) | (~X & Z);
        }

        static public uint G(uint X, uint Y, uint Z)
        {
            return (X & Z) | (Y & ~Z);
        }

        static public uint H(uint X, uint Y, uint Z)
        {
            return X ^ Y ^ Z;
        }

        static public uint K(uint X, uint Y, uint Z)
        {
            return Y ^ (X | ~Z);
        }

        static public void step(byte[] array, uint a, uint b, uint c, uint d, uint k, int s, int i)
        {
            uint x = (uint)array[k * 4 + 3] * 16777216 + (uint)array[k * 4 + 2] * 65536 + (uint)array[k * 4 + 1] * 256 + (uint)array[k * 4];
            uint tek = a;
            if (RoundCount == f) tek += F(b, c, d);
            else if (RoundCount == g) tek += G(b, c, d);
            else if (RoundCount == h) tek += H(b, c, d);
            else tek += K(b, c, d);
            tek += x;
            tek += T[i - 1];
            tek = Rot(tek, s);
            tek += b;
            if (a == A) A = tek;
            else if (a == B) B = tek;
            else if (a == C) C = tek;
            else D = tek;
        }

        static public void round(byte[] array)
        {
            uint AA = A;
            uint BB = B;
            uint CC = C;
            uint DD = D;
            RoundCount = 1;
            step(array, A, B, C, D, 0, 7, 1);
            step(array, D, A, B, C, 1, 12, 2);
            step(array, C, D, A, B, 2, 17, 3);
            step(array, B, C, D, A, 3, 22, 4);
            step(array, A, B, C, D, 4, 7, 5);
            step(array, D, A, B, C, 5, 12, 6);
            step(array, C, D, A, B, 6, 17, 7);
            step(array, B, C, D, A, 7, 22, 8);
            step(array, A, B, C, D, 8, 7, 9);
            step(array, D, A, B, C, 9, 12, 10);
            step(array, C, D, A, B, 10, 17, 11);
            step(array, B, C, D, A, 11, 22, 12);
            step(array, A, B, C, D, 12, 7, 13);
            step(array, D, A, B, C, 13, 12, 14);
            step(array, C, D, A, B, 14, 17, 15);
            step(array, B, C, D, A, 15, 22, 16);
            RoundCount++;
            step(array, A, B, C, D, 1, 5, 17);
            step(array, D, A, B, C, 6, 9, 18);
            step(array, C, D, A, B, 11, 14, 19);
            step(array, B, C, D, A, 0, 20, 20);
            step(array, A, B, C, D, 5, 5, 21);
            step(array, D, A, B, C, 10, 9, 22);
            step(array, C, D, A, B, 15, 14, 23);
            step(array, B, C, D, A, 4, 20, 24);
            step(array, A, B, C, D, 9, 5, 25);
            step(array, D, A, B, C, 14, 9, 26);
            step(array, C, D, A, B, 3, 14, 27);
            step(array, B, C, D, A, 8, 20, 28);
            step(array, A, B, C, D, 13, 5, 29);
            step(array, D, A, B, C, 2, 9, 30);
            step(array, C, D, A, B, 7, 14, 31);
            step(array, B, C, D, A, 12, 20, 32);
            RoundCount++;
            step(array, A, B, C, D, 5, 4, 33);
            step(array, D, A, B, C, 8, 11, 34);
            step(array, C, D, A, B, 11, 16, 35);
            step(array, B, C, D, A, 14, 23, 36);
            step(array, A, B, C, D, 1, 4, 37);
            step(array, D, A, B, C, 4, 11, 38);
            step(array, C, D, A, B, 7, 16, 39);
            step(array, B, C, D, A, 10, 23, 40);
            step(array, A, B, C, D, 13, 4, 41);
            step(array, D, A, B, C, 0, 11, 42);
            step(array, C, D, A, B, 3, 16, 43);
            step(array, B, C, D, A, 6, 23, 44);
            step(array, A, B, C, D, 9, 4, 45);
            step(array, D, A, B, C, 12, 11, 46);
            step(array, C, D, A, B, 15, 16, 47);
            step(array, B, C, D, A, 2, 23, 48);
            RoundCount++;
            step(array, A, B, C, D, 0, 6, 49);
            step(array, D, A, B, C, 7, 10, 50);
            step(array, C, D, A, B, 14, 15, 51);
            step(array, B, C, D, A, 5, 21, 52);
            step(array, A, B, C, D, 12, 6, 53);
            step(array, D, A, B, C, 3, 10, 54);
            step(array, C, D, A, B, 10, 15, 55);
            step(array, B, C, D, A, 1, 21, 56);
            step(array, A, B, C, D, 8, 6, 57);
            step(array, D, A, B, C, 15, 10, 58);
            step(array, C, D, A, B, 6, 15, 59);
            step(array, B, C, D, A, 13, 21, 60);
            step(array, A, B, C, D, 4, 6, 61);
            step(array, D, A, B, C, 11, 10, 62);
            step(array, C, D, A, B, 2, 15, 63);
            step(array, B, C, D, A, 9, 21, 64);
            A += AA;
            B += BB;
            C += CC;
            D += DD;
        }

        static public void md5(byte[] text2)
        {
            A = 0x67452301;
            B = 0xEFCDAB89;
            C = 0x98BADCFE;
            D = 0X10325476;
            uint[] mass = new uint[16];
            byte[] text = new byte[text2.Length + 1];
            Buffer.BlockCopy(text2, 0, text, 0, text2.Length);
            text[text.Length - 1] = 0x80;
            while(text.Length % 64 != 56)
            {
                byte[] temp = new byte[text.Length + 1];
                Buffer.BlockCopy(text, 0, temp, 0, text.Length);
                temp[text.Length] = 0;
                text = temp;
            }

            
            uint Length = (uint)text2.Length * 8;
            byte[] temp2 = new byte[text.Length + 8];
            Buffer.BlockCopy(text, 0, temp2, 0, text.Length);
            for (int p = 0; p < 8; p++)
            {
                temp2[temp2.Length - 8 + p] = (byte)(Length % Math.Pow(2, 8 * (p + 1)) / Math.Pow(2, 8 * p));
            }
            text = temp2;
            //Console.WriteLine("Получен поток байт для работы:");
            //Console.WriteLine(string.Join(" ", text));
            int count = 0;
            do
            {
                byte[] send = new byte[64];
                Buffer.BlockCopy(text, count * 64, send, 0, 64);
                round(send);
                count++;
            } while (count * 64 < text.Length);
        }

        static public IEnumerable<bool> GetBits(byte b)
        {
            for (int i = 0; i < 8; i++)
            {
                yield return (b & 0x80) != 0;
                b *= 2;
            }
        }

        static void Main(string[] args)
        {
            /*Console.WriteLine("Режимы работы:");
            Console.WriteLine("1) F, G, H, K");
            Console.WriteLine("2) G, H, K, F");
            Console.WriteLine("3) H, K, F, G");
            Console.WriteLine("4) K, F, G, H");
            Console.Write("Выберите режим работы: ");
            int choice = 0;
            int.TryParse(Console.ReadLine(), out choice);
            if(choice == 2)
            {
                f = 4;
                g = 1;
                h = 2;
                k = 3;
            }
            else if(choice == 3)
            {
                f = 3;
                g = 4;
                h = 1;
                k = 2;
            }
            else if(choice == 4)
            {
                f = 2;
                g = 3;
                h = 4;
                k = 1;
            }
            else
            {
                f = 1;
                g = 2;
                h = 3;
                k = 4;
            }
            Console.Write("Введите сообщение: ");
            string text = Console.ReadLine();
            md5(Encoding.ASCII.GetBytes(text));
            Console.WriteLine("Получен следующий md5 хэш:");
            Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", A % 256, A % 65536 / 256, A % 16777216 / 65536, A / 16777216);
            Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", B % 256, B % 65536 / 256, B % 16777216 / 65536, B / 16777216);
            Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", C % 256, C % 65536 / 256, C % 16777216 / 65536, C / 16777216);
            Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", D % 256, D % 65536 / 256, D % 16777216 / 65536, D / 16777216);*/
            for (int u = 0; u < 4; u++)
            {
                if(u == 0)
                {
                    Console.WriteLine("_____________________________________________");
                    Console.WriteLine();
                    Console.WriteLine("Режим F, G, H, K");
                    Console.WriteLine();
                    f = 1;
                    g = 2;
                    h = 3;
                    k = 4;
                }
                else if (u == 1)
                {
                    Console.WriteLine("_____________________________________________");
                    Console.WriteLine();
                    Console.WriteLine("Режим G, H, K, F");
                    Console.WriteLine();
                    f = 4;
                    g = 1;
                    h = 2;
                    k = 3;
                }
                else if (u == 2)
                {
                    Console.WriteLine("_____________________________________________");
                    Console.WriteLine();
                    Console.WriteLine("Режим H, K, F, G");
                    Console.WriteLine();
                    f = 3;
                    g = 4;
                    h = 1;
                    k = 2;
                }
                else
                {
                    Console.WriteLine("_____________________________________________");
                    Console.WriteLine();
                    Console.WriteLine("Режим K, F, G, H");
                    Console.WriteLine();
                    f = 2;
                    g = 3;
                    h = 4;
                    k = 1;
                }
                string[] mass = { "aaaaa", "aaaab", "aaaba", "aabaa", "abaaa", "baaaa" };
                byte[][] hashes = new byte[6][];
                Console.WriteLine();
                Console.WriteLine("Получены следующие хэши:");
                for (int p = 0; p < 6; p++)
                {
                    hashes[p] = new byte[16];
                    Console.Write("md5(" + mass[p] + ") = ");
                    md5(Encoding.ASCII.GetBytes(mass[p]));
                    hashes[p][0] = (byte)(A % 256);
                    hashes[p][1] = (byte)(A % 65536 / 256);
                    hashes[p][2] = (byte)(A % 16777216 / 65536);
                    hashes[p][3] = (byte)(A / 16777216);
                    hashes[p][4] = (byte)(B % 256);
                    hashes[p][5] = (byte)(B % 65536 / 256);
                    hashes[p][6] = (byte)(B % 16777216 / 65536);
                    hashes[p][7] = (byte)(B / 16777216);
                    hashes[p][8] = (byte)(C % 256);
                    hashes[p][9] = (byte)(C % 65536 / 256);
                    hashes[p][10] = (byte)(C % 16777216 / 65536);
                    hashes[p][11] = (byte)(C / 16777216);
                    hashes[p][12] = (byte)(D % 256);
                    hashes[p][13] = (byte)(D % 65536 / 256);
                    hashes[p][14] = (byte)(D % 16777216 / 65536);
                    hashes[p][15] = (byte)(D / 16777216);
                    for (int q = 0; q < 16; q++)
                    {
                        Console.Write("{0:x2}", hashes[p][q]);
                    }
                    Console.WriteLine();
                }
                bool[][] bits = new bool[6][];
                for (int p = 0; p < 6; p++)
                {
                    bits[p] = hashes[p].SelectMany(GetBits).ToArray();
                }
                uint[,] Hamming = new uint[6, 6];
                for (int p = 0; p < 6; p++)
                {
                    for (int q = 0; q < 6; q++)
                    {
                        if (p == q) continue;
                        Hamming[p, q] = 0;
                        for (int s = 0; s < 128; s++)
                        {
                            if (bits[p][s] != bits[q][s]) Hamming[p, q]++;
                        }
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Получена следующая таблица:");
                double mr = 0;
                for (int p = 0; p < 6; p++)
                {
                    for (int q = 0; q < 6; q++)
                    {
                        if (p == q) Console.Write(" - ");
                        else
                        {
                            mr += ((double)Hamming[p, q] / 30);
                            Console.Write(Hamming[p, q] + " ");
                        }
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
                Console.WriteLine("mr = " + mr);
                double r = 0;
                for (int p = 0; p < 6; p++)
                {
                    for (int q = 0; q < 6; q++)
                    {
                        if (p != q)
                        {
                            r += ((mr - (double)Hamming[p, q]) * (mr - (double)Hamming[p, q]) / (double)30);
                        }
                    }
                }
                r = Math.Sqrt(r);
                Console.WriteLine("r = " + r);
            }
            Console.Read();
        }
    }
}
