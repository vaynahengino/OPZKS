﻿using MD5;
using System;
using System.Linq;
using System.Text;

namespace ca
{
  class Program
  {
    static void Main(string[] args)
    {
      MD5Processor md5 = new MD5Processor();
      int regime = 0;

      for (int u = 0; u < 4; u++)
      {
        if (u == 0)
        {
          Console.WriteLine("_____________________________________________");
          Console.WriteLine();
          Console.WriteLine("Режим F, G, H, K");
          Console.WriteLine();
          regime = 0;
        }
        else if (u == 1)
        {
          Console.WriteLine("_____________________________________________");
          Console.WriteLine();
          Console.WriteLine("Режим G, H, K, F");
          Console.WriteLine();
          regime = 1;
        }
        else if (u == 2)
        {
          Console.WriteLine("_____________________________________________");
          Console.WriteLine();
          Console.WriteLine("Режим H, K, F, G");
          Console.WriteLine();
          regime = 2;
        }
        else
        {
          Console.WriteLine("_____________________________________________");
          Console.WriteLine();
          Console.WriteLine("Режим K, F, G, H");
          Console.WriteLine();
          regime = 3;
        }

        string[] messages = { "aaaaa", "aaaab", "aaaba", "aabaa", "abaaa", "baaaa" };
        byte[][] hashes = new byte[6][];
        Console.WriteLine();
        Console.WriteLine("Получены следующие хэши:");
        for (int p = 0; p < 6; p++)
        {
          hashes[p] = new byte[16];
          Console.Write($"md5({messages[p]}) = ");
          md5.Calculate(Encoding.ASCII.GetBytes(messages[p]), regime);
          hashes[p][0] = (byte)(md5.A % 256);
          hashes[p][1] = (byte)(md5.A % 65536 / 256);
          hashes[p][2] = (byte)(md5.A % 16777216 / 65536);
          hashes[p][3] = (byte)(md5.A / 16777216);
          hashes[p][4] = (byte)(md5.B % 256);
          hashes[p][5] = (byte)(md5.B % 65536 / 256);
          hashes[p][6] = (byte)(md5.B % 16777216 / 65536);
          hashes[p][7] = (byte)(md5.B / 16777216);
          hashes[p][8] = (byte)(md5.C % 256);
          hashes[p][9] = (byte)(md5.C % 65536 / 256);
          hashes[p][10] = (byte)(md5.C % 16777216 / 65536);
          hashes[p][11] = (byte)(md5.C / 16777216);
          hashes[p][12] = (byte)(md5.D % 256);
          hashes[p][13] = (byte)(md5.D % 65536 / 256);
          hashes[p][14] = (byte)(md5.D % 16777216 / 65536);
          hashes[p][15] = (byte)(md5.D / 16777216);
          for (int q = 0; q < 16; q++)
          {
            Console.Write("{0:x2}", hashes[p][q]);
          }
          Console.WriteLine();
        }
        bool[][] bits = new bool[6][];
        for (int p = 0; p < 6; p++)
        {
          bits[p] = hashes[p].SelectMany(MD5Processor.GetBits).ToArray();
        }
        uint[,] Hamming = new uint[6, 6];
        for (int p = 0; p < 6; p++)
        {
          for (int q = 0; q < 6; q++)
          {
            if (p == q) continue;
            Hamming[p, q] = 0;
            for (int s = 0; s < 128; s++)
            {
              if (bits[p][s] != bits[q][s]) Hamming[p, q]++;
            }
          }
        }
        Console.WriteLine();
        Console.WriteLine("Получена следующая таблица:");
        double mr = 0;
        for (int p = 0; p < 6; p++)
        {
          for (int q = 0; q < 6; q++)
          {
            if (p == q) Console.Write(" - ");
            else
            {
              mr += ((double)Hamming[p, q] / 30);
              Console.Write(Hamming[p, q] + " ");
            }
          }
          Console.WriteLine();
        }
        Console.WriteLine();
        Console.WriteLine($"Mr = {mr}");
        double r = 0;
        for (int p = 0; p < 6; p++)
        {
          for (int q = 0; q < 6; q++)
          {
            if (p != q)
            {
              r += ((mr - (double)Hamming[p, q]) * (mr - (double)Hamming[p, q]) / (double)30);
            }
          }
        }
        r = Math.Sqrt(r);
        Console.WriteLine($"r = {r}");
      }
      Console.ReadKey();
    }
  }
}
