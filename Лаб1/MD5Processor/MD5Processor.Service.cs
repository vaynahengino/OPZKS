﻿using System;
using System.Collections.Generic;

namespace MD5
{
  public partial class MD5Processor
  {
    public static IEnumerable<bool> GetBits(byte b)
    {
      for (int i = 0; i < 8; i++)
      {
        yield return (b & 0x80) != 0;
        b *= 2;
      }
    }

    public uint ToWord(string mes)
    {
      if (mes.Length == 8 && this.ByteIsValid(mes))
      {
        uint res = 0;
        uint temp = 0;
        for (int i = 0; i < mes.Length; i++)
        {
          switch (char.ToUpperInvariant(mes[i]))
          {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9': temp = uint.Parse(mes[i].ToString()); break;
            case 'A': temp = 10; break;
            case 'B': temp = 11; break;
            case 'C': temp = 12; break;
            case 'D': temp = 13; break;
            case 'E': temp = 14; break;
            case 'F': temp = 15; break;
          }
          res <<= 4;
          res |= temp;
        }
        return res;
      }
      return 0;
    }

    private bool ByteIsValid(string bytes)
    {
      foreach (char c in bytes)
      {
        if (!(c >= '0' && c <= '9' || char.ToUpperInvariant(c) >= 'A' && char.ToUpperInvariant(c) <= 'F')) return false;
      }
      return true;
    }
  }
}
