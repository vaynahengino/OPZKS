﻿using System;

namespace MD5
{
  public partial class MD5Processor
  {
    public static uint F(uint x, uint y, uint z)
    {
      return (x & y) | (~x & z);
    }

    public static uint G(uint x, uint y, uint z)
    {
      return (x & z) | (y & ~z);
    }

    public static uint H(uint x, uint y, uint z)
    {
      return x ^ y ^ z;
    }

    public static uint K(uint x, uint y, uint z)
    {
      return y ^ (x | ~z);
    }
  }
}
