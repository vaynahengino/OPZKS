﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Numerics;
using System.Threading.Tasks;
using System.Threading;

namespace Bob
{
    class Program
    {
        private static BigInteger e = 65537;
        private static int MillerRabinCount = 100;

        static Socket talk;
        static void Say(string message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static void Say(int message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static void Say(BigInteger message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static BigInteger Listen()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            BigInteger tek;
            BigInteger.TryParse(message, out tek);
            return tek;
        }

        static BigInteger getRandom(int length)
        {
            byte[] data = new byte[length];
            while (true)
            {
                Random random = new Random((int)DateTime.Now.Ticks);
                random.NextBytes(data);
                if (new BigInteger(data) > 1) break;
            }
            return new BigInteger(data);
        }

        private static bool Witness(BigInteger a, BigInteger n)
        {

            BigInteger d = n - 1;
            int s = 0;
            while (d % 2 == 0)
            {
                s++;
                d /= 2;
            }

            if (BigInteger.ModPow(a, d, n) == 1) return true;

            for (int p = 0; p <= s - 1; p++)
            {
                if (BigInteger.ModPow(a, BigInteger.Pow(2, p) * d, n) == n - 1) return true;
            }
            return false;
        }

        public static bool IsPrimeMillerRabin(BigInteger n, int s, int length)
        {
            if (n % 2 == 0 || n <= 1) return false;
            for (int j = 0; j < s; j++)
            {
                BigInteger a = 0;
                while (true)
                {
                    a = getRandom(length);
                    if (a < n) break;
                }
                if (!Witness(a, n))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool PseudoPrime(BigInteger n)
        {
            BigInteger modularExponentiation = ModularExponentiation(2, BigInteger.Subtract(n, 1), n);
            if (!modularExponentiation.IsOne)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static BigInteger ModularExponentiation(BigInteger a, BigInteger b, BigInteger n)
        {
            return BigInteger.ModPow(a, b, n);
        }

        private static BigInteger GCD(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }
            BigInteger x1, y1;
            BigInteger d = GCD(b % a, a, out x1, out y1);
            x = y1 - (b / a) * x1;
            y = x1;
            return d;
        }
        private static BigInteger Foo(BigInteger a, BigInteger m)
        {
            BigInteger x, y;
            BigInteger g = GCD(a, m, out x, out y);
            if (g != 1)
                throw new ArgumentException();
            return (x % m + m) % m;
        }

        static void Main(string[] args)
        {
            BigInteger mn1;
            BigInteger mn2;
            BigInteger n;
            BigInteger SecretD;
            while (true)
            {
                while (true)
                {
                    mn1 = getRandom(16);
                    bool good = true;
                    for (int r = 2; r < 10000; r++)
                    {
                        if (mn1 % r == 0)
                        {
                            good = false;
                            break;
                        }
                    }
                    if (good == false) continue;
                    if (PseudoPrime(mn1) && IsPrimeMillerRabin(mn1, MillerRabinCount, 16)) break;
                }
                while (true)
                {
                    mn2 = getRandom(16);
                    bool good = true;
                    for (int r = 2; r < 10000; r++)
                    {
                        if (mn2 % r == 0)
                        {
                            good = false;
                            break;
                        }
                    }
                    if (good == false) continue;
                    if (PseudoPrime(mn2) && IsPrimeMillerRabin(mn2, MillerRabinCount, 16)) break;
                }
                n = mn1 * mn2;
                if (n.ToByteArray().Length != 32) continue;
                BigInteger eiler = (mn1 - 1) * (mn2 - 1);
                SecretD = Foo(e, eiler);
                break;
            }

            Console.WriteLine("IP-Address: " + Dns.GetHostByName(Dns.GetHostName()).AddressList[0]);
            Console.WriteLine("Привет, я Боб. Я открываю 11111 порт для связи с Алисой. Жду подключения");
            IPAddress ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11111);
            Socket talking = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            talking.Bind(ipEndPoint);
            talking.Listen(1);
            talk = talking.Accept();
            BigInteger P = Listen();
            BigInteger W = Listen();
            BigInteger WXA = Listen();
            Console.WriteLine("P = " + P);
            Console.WriteLine("W = " + W);
            Console.WriteLine("Incoming message (W ^ XA mod P): " + WXA);
            BigInteger XB = getRandom(5);
            Console.WriteLine("XB = " + XB);
            BigInteger WXB = BigInteger.ModPow(W, XB, P);
            Console.WriteLine("Send message (W ^ XB mod P): " + WXB);
            Say(WXB);
            BigInteger ishMes;
            BigInteger.TryParse(WXA.ToString() + WXB.ToString(), out ishMes);
            BigInteger Mes = BigInteger.ModPow(ishMes, SecretD, n);
            BigInteger KAB = BigInteger.ModPow(WXA, XB, P);
            Mes = Mes ^ KAB;
            Console.WriteLine("Send cipher ECP: " + Mes);
            Say(Mes);
            BigInteger IncMes = Listen();
            Console.WriteLine("Incoming cipher ECP: " + IncMes);

            Say(n);
            BigInteger Open = Listen();

            IncMes = IncMes ^ KAB;
            IncMes = BigInteger.ModPow(IncMes, e, Open);
            if (IncMes == ishMes)
            {
                Console.WriteLine("Alice was validated");
            }
            else
            {
                Console.WriteLine("It's not an Alice");
            }


            Console.Read();
        }
    }
}
