﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;
using System.IO;
using MD5;

namespace ca
{
  class Program
  {
    static public IEnumerable<bool> GetBits(byte b)
    {
      for (int i = 0; i < 8; i++)
      {
        yield return (b & 0x80) != 0;
        b *= 2;
      }
    }

    static BigInteger GetRandomNumber(int length)
    {
      byte[] data = new byte[length];
      while (true)
      {
        Random random = new Random((int)DateTime.Now.Ticks);
        random.NextBytes(data);
        if (new BigInteger(data) > 1) break;
      }
      return new BigInteger(data);
    }

    static BigInteger primitiv(BigInteger p, int length)
    {
      if (p == 2) return 1;
      BigInteger q = 2;
      BigInteger r = (p - 1) / q;
      while (true)
      {
        BigInteger g = 2;
        while (true)
        {
          g = GetRandomNumber(length);
          if (g < p - 1) break;
        }
        if (BigInteger.ModPow(g, r, p) != 1 && BigInteger.ModPow(g, 2, p) != 1) return g;
      }
    }

    private static bool Witness(BigInteger a, BigInteger n)
    {

      BigInteger d = n - 1;
      int s = 0;
      while (d % 2 == 0)
      {
        s++;
        d /= 2;
      }

      if (BigInteger.ModPow(a, d, n) == 1) return true;

      for (int p = 0; p <= s - 1; p++)
      {
        if (BigInteger.ModPow(a, BigInteger.Pow(2, p) * d, n) == n - 1) return true;
      }
      return false;
    }

    public static bool IsPrimeByMillerRabin(BigInteger n, int s, int length)
    {
      if (n % 2 == 0 || n <= 1) return false;
      for (int j = 0; j < s; j++)
      {
        BigInteger a = 0;
        while (true)
        {
          a = GetRandomNumber(length);
          if (a < n) break;
        }
        if (!Witness(a, n))
        {
          return false;
        }
        Console.WriteLine("Testing: " + ((double)j + 1) / (double)s * 100 + "%");
      }
      return true;
    }

    static BigInteger GetGCD(BigInteger num1, BigInteger num2)
    {
      while (num1 != num2)
      {
        if (num1 > num2)
          num1 = num1 - num2;

        if (num2 > num1)
          num2 = num2 - num1;
      }
      return num1;
    }

    private static BigInteger Foo(BigInteger a, BigInteger m)
    {
      BigInteger x, y;
      BigInteger g = GCD(a, m, out x, out y);
      if (g != 1)
        throw new ArgumentException();
      return (x % m + m) % m;
    }

    private static BigInteger GCD(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
    {
      if (a == 0)
      {
        x = 0;
        y = 1;
        return b;
      }
      BigInteger x1, y1;
      BigInteger d = GCD(b % a, a, out x1, out y1);
      x = y1 - (b / a) * x1;
      y = x1;
      return d;
    }

    static void Main(string[] args)
    {
      MD5Processor md5 = new MD5Processor();

      int length = 40;
      int regime = 0;
      Console.WriteLine("Генерация ключей");
      Console.WriteLine();
      Console.WriteLine("Поиск простого числа");
      BigInteger module = 0;
      while (true)
      {
        module = GetRandomNumber(length);
        Console.WriteLine(module);
        bool answer = IsPrimeByMillerRabin(module, 100, length);
        Console.WriteLine("Результат проверки на простоту: " + answer);
        if (answer) break;
      }
      Console.WriteLine();
      BigInteger prim = primitiv(module, length);
      BigInteger x = 2;
      while (true)
      {
        x = GetRandomNumber(length - 1);
        if (x < module) break;
      }
      BigInteger y = BigInteger.ModPow(prim, x, module);
      Console.WriteLine();
      Console.WriteLine();
      Console.WriteLine("Открытый ключ: ");
      Console.WriteLine("P = " + module);
      Console.WriteLine("G = " + prim);
      Console.WriteLine("Y = " + y);
      Console.WriteLine();
      Console.WriteLine("Закрытый ключ: " + x);
      Console.WriteLine();
      File.WriteAllText(@".\OpenKey.txt", module + " " + prim + " " + y);
      Console.WriteLine("Файлы ключей успешно созданы");
      TimeSpan ts;
      DateTime start = DateTime.Now;
      for (regime = 0; regime <= 3; regime++)
      {
        start = DateTime.Now;
        Console.WriteLine("Подписывание файла Text.txt методом \"ЦП с добавлением\"");
        // string text = File.ReadAllText(@".\Mandriva.7z");
        string text = File.ReadAllText(@".\Text.txt");
        md5.Calculate(Encoding.ASCII.GetBytes(text), regime);
        Console.WriteLine("Получен следующий md5 хэш:");
        Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", md5.A % 256, md5.A % 65536 / 256, md5.A % 16777216 / 65536, md5.A / 16777216);
        Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", md5.B % 256, md5.B % 65536 / 256, md5.B % 16777216 / 65536, md5.B / 16777216);
        Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", md5.C % 256, md5.C % 65536 / 256, md5.C % 16777216 / 65536, md5.C / 16777216);
        Console.WriteLine("{0:x2}{1:x2}{2:x2}{3:x2}", md5.D % 256, md5.D % 65536 / 256, md5.D % 16777216 / 65536, md5.D / 16777216);
        Console.WriteLine();
        Console.WriteLine("Открытый ключ: ");
        Console.WriteLine("P = " + module);
        Console.WriteLine("G = " + prim);
        Console.WriteLine("Y = " + y);
        Console.WriteLine();
        Console.WriteLine("Закрытый ключ: " + x);
        Console.WriteLine();
        BigInteger seans = 2;
        while (true)
        {
          seans = GetRandomNumber(length - 1);
          if (seans < module - 1 && GetGCD(seans, module - 1) == 1) break;
        }
        BigInteger r = BigInteger.ModPow(prim, seans, module);
        BigInteger hash = md5.D / 16777216;
        hash += (BigInteger)(md5.D % 16777216 / 65536) << 8;
        hash += (BigInteger)(md5.D % 65536 / 256) << 16;
        hash += (BigInteger)(md5.D % 256) << 24;
        hash += (BigInteger)(md5.C / 16777216) << 32;
        hash += (BigInteger)(md5.C % 16777216 / 65536) << 40;
        hash += (BigInteger)(md5.C % 65536 / 256) << 48;
        hash += (BigInteger)(md5.C % 256) << 56;
        hash += (BigInteger)(md5.B / 16777216) << 64;
        hash += (BigInteger)(md5.B % 16777216 / 65536) << 72;
        hash += (BigInteger)(md5.B % 65536 / 256) << 80;
        hash += (BigInteger)(md5.B % 256) << 88;
        hash += (BigInteger)(md5.A / 16777216) << 96;
        hash += (BigInteger)(md5.A % 16777216 / 65536) << 104;
        hash += (BigInteger)(md5.A % 65536 / 256) << 112;
        hash += (BigInteger)(md5.A % 256) << 120;
        BigInteger s = (hash - x * r) * Foo(seans, module - 1) % (module - 1);
        if (s < 0)
        {
          s = (module - 1 + s) % module;
        }
        ts = DateTime.Now - start;

        Console.WriteLine();
        Console.WriteLine("Получена подпись:");
        Console.WriteLine("r = " + r);
        Console.WriteLine("s = " + s); ;

        Console.WriteLine();
        Console.WriteLine("Проверка подписи");
        text = File.ReadAllText(@".\Text.txt");
        md5.Calculate(Encoding.ASCII.GetBytes(text), regime);
        Console.WriteLine("Получен следующий md5 хэш:");
        Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", md5.A % 256, md5.A % 65536 / 256, md5.A % 16777216 / 65536, md5.A / 16777216);
        Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", md5.B % 256, md5.B % 65536 / 256, md5.B % 16777216 / 65536, md5.B / 16777216);
        Console.Write("{0:x2}{1:x2}{2:x2}{3:x2}", md5.C % 256, md5.C % 65536 / 256, md5.C % 16777216 / 65536, md5.C / 16777216);
        Console.WriteLine("{0:x2}{1:x2}{2:x2}{3:x2}", md5.D % 256, md5.D % 65536 / 256, md5.D % 16777216 / 65536, md5.D / 16777216);
        Console.WriteLine();
        hash = md5.D / 16777216;
        hash += (BigInteger)(md5.D % 16777216 / 65536) << 8;
        hash += (BigInteger)(md5.D % 65536 / 256) << 16;
        hash += (BigInteger)(md5.D % 256) << 24;
        hash += (BigInteger)(md5.C / 16777216) << 32;
        hash += (BigInteger)(md5.C % 16777216 / 65536) << 40;
        hash += (BigInteger)(md5.C % 65536 / 256) << 48;
        hash += (BigInteger)(md5.C % 256) << 56;
        hash += (BigInteger)(md5.B / 16777216) << 64;
        hash += (BigInteger)(md5.B % 16777216 / 65536) << 72;
        hash += (BigInteger)(md5.B % 65536 / 256) << 80;
        hash += (BigInteger)(md5.B % 256) << 88;
        hash += (BigInteger)(md5.A / 16777216) << 96;
        hash += (BigInteger)(md5.A % 16777216 / 65536) << 104;
        hash += (BigInteger)(md5.A % 65536 / 256) << 112;
        hash += (BigInteger)(md5.A % 256) << 120;
        Console.WriteLine("Открытый ключ: ");
        Console.WriteLine("P = " + module);
        Console.WriteLine("G = " + prim);
        Console.WriteLine("Y = " + y);

        Console.WriteLine();
        Console.WriteLine("Подпись");
        Console.WriteLine("r = " + r);
        Console.WriteLine("s = " + s);
        Console.WriteLine();

        if ((BigInteger.ModPow(y, r, module) * BigInteger.ModPow(r, s, module)) % module == BigInteger.ModPow(prim, hash, module)) Console.WriteLine("Верификация подписи прошла успешно");
        else Console.WriteLine("Подпись не совпала с полученной");

        Console.WriteLine($"Файл подписан за: {ts} времени");

        Console.WriteLine("=====================================================");
      }
      Console.ReadKey();
    }
  }
}
