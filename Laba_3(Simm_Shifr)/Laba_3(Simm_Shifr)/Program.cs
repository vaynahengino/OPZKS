﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace Laba_3_Simm_Shifr_
{
    class Program
    {
        static int[][] transpont(int[][] ish)
        {
            int[][] itog = new int[ish[0].Length][];
            for(int p = 0; p < itog.Length; p++)
            {
                itog[p] = new int[ish.Length];
            }
            for(int p = 0; p < itog.Length; p++)
            {
                for(int q = 0; q < itog[p].Length; q++)
                {
                    itog[p][q] = ish[q][p];
                }
            }
            return itog;
        }

        static int[][] multiply_matrix(int[][] a, int[][] b)
        {
            int[][] itog = new int[a.Length][];
            for(int p = 0; p < a.Length; p++)
            {
                itog[p] = new int[b[p].Length];
            }
            for(int p = 0; p < itog.Length; p++)
            {
                for(int q = 0; q < itog[p].Length; q++)
                {
                    int tek = 0;
                    for(int r = 0; r < itog[p].Length; r++)
                    {
                        tek += a[p][r] * b[r][q];
                    }
                    itog[p][q] = tek;
                }
            }
            return itog;
        }

        static string Encode_Perestanovka(string text, int[][] matrix)
        {
            string itog = "";
            for (int r = 0; r < text.Length; r+=100)
            {
                int[][] tek = new int[matrix.Length][];
                for (int p = 0; p < tek.Length; p++)
                {
                    tek[p] = new int[matrix[p].Length];
                    for (int q = 0; q < tek[p].Length; q++)
                    {
                        tek[p][q] = (int)(text[p * tek[p].Length + q + r]);
                    }
                }
                int[][] after = multiply_matrix(multiply_matrix(matrix, tek), transpont(matrix));
                
                for (int p = 0; p < after.Length; p++)
                {
                    for (int q = 0; q < after[p].Length; q++)
                    {
                        itog += (char)after[p][q];
                    }
                }
            }
            return itog;
        }

        static string Decode_Perestanovka(string text, int[][] matrix)
        {
            string itog = "";
            for (int r = 0; r < text.Length; r+=100)
            {
                int[][] tek = new int[matrix.Length][];
                for (int p = 0; p < tek.Length; p++)
                {
                    tek[p] = new int[matrix[p].Length];
                    for (int q = 0; q < tek[p].Length; q++)
                    {
                        tek[p][q] = (int)(text[p * tek[p].Length + q + r]);
                    }
                }
                int[][] after = multiply_matrix(multiply_matrix(transpont(matrix), tek), matrix);
                
                for (int p = 0; p < after.Length; p++)
                {
                    for (int q = 0; q < after[p].Length; q++)
                    {
                        itog += (char)after[p][q];
                    }
                }
            }
            return itog;
        }

        static string Encode_Zamena(string text, int key)
        {
            string itog = "";
            for(int p = 0; p < text.Length; p++)
            {
                itog += (char)(((int)text[p] + key) % 256);
            }
            return itog;
        }

        static string Decode_Zamena(string text, int key)
        {
            string itog = "";
            for (int p = 0; p < text.Length; p++)
            {
                int tek = (int)text[p] - key;
                if (tek < 0) tek += 256;
                itog += (char)tek;
            }
            return itog;
        }

        static string Gamma(string text, string key)
        {
            string itog = "";
            for(int p = 0; p < text.Length; p += key.Length)
            {
                for(int q = 0; q < key.Length; q++)
                {
                    itog += (char)(text[p + q] ^ key[q]);
                }
            }
            return itog;
        }

        static int[][] getKeyPerestanovka(int[][] ish, int[][] per, int step)
        {
            if (step == 0) return ish;
            int[][] itog = new int[ish.Length][];
            for(int p = 0; p < ish.Length; p++)
            {
                itog[p] = new int[ish[p].Length];
                for(int q = 0; q < ish[p].Length; q++)
                {
                    itog[p][q] = ish[p][q];
                }
            }
            for(int p = 0; p < step; p++)
            {
                itog = multiply_matrix(per, itog);
            }
            return itog;
        }

        static int getKeyZamena(int ish, int s, int step)
        {
            return (ish + s * step) % 256;
        }

        static string getKeyGamma(string ish, int[][] per, int step)
        {
            if (step == 0) return ish;
            int[][] before = new int[1][];
            before[0] = new int[ish.Length];
            for(int p = 0; p < ish.Length; p++)
            {
                before[0][p] = (int)ish[p];
            }
            for (int r = 0; r < step; r++)
            {
                for (int p = 0; p < ish.Length; p++)
                {
                    int[][] temp = new int[1][];
                    temp[0] = new int[8];
                    int tek = before[0][p];
                    int pos = 7;
                    while (tek > 0)
                    {
                        if (tek % 2 == 0) temp[0][pos] = 0;
                        else temp[0][pos] = 1;
                        tek /= 2;
                        pos--;
                    }
                    if (pos >= 0)
                    {
                        for (int q = 0; q <= pos; q++)
                        {
                            temp[0][q] = 0;
                        }
                    }
                    temp = multiply_matrix(temp, per);
                    tek = 0;
                    for (int q = 0; q < 8; q++)
                    {
                        tek += (int)(Math.Pow(2, 7 - q) * temp[0][q]);
                    }
                    before[0][p] = tek;
                }
            }
            string itog = "";
            for(int p = 0; p < before[0].Length; p++)
            {
                itog += (char)before[0][p];
            }
            return itog;
        }

        static string genGammaIsh()
        {
            string res = "";
            Random rnd = new Random();
            for(int p = 0; p < 16; p++)
            {
                int tek = rnd.Next(1, 255);
                res += (char)tek;
            }
            return res;
        }

        static int genZamenaIsh()
        {
            Random rnd = new Random();
            return rnd.Next(0, 255);
        }

        static int[][] genPerestanovkaIsh()
        {
            bool[] isp = new bool[10];
            for(int p = 0; p < 10; p++)
            {
                isp[p] = false;
            }
            Random rnd = new Random();
            int[][] itog = new int[10][];
            for(int p = 0; p < 10; p++)
            {
                itog[p] = new int[10];
                for(int q = 0; q < 10; q++)
                {
                    itog[p][q] = 0;
                }
            }
            for(int p = 0; p < 10; p++)
            {
                int tek = 0;
                do
                {
                    tek = rnd.Next(0, 10);
                } while (isp[tek] != false);
                itog[p][tek] = 1;
                isp[tek] = true;
            }
            return itog;
        }

        static int[][] genGammaMod()
        {
            bool[] isp = new bool[8];
            for (int p = 0; p < 8; p++)
            {
                isp[p] = false;
            }
            Random rnd = new Random();
            int[][] itog = new int[8][];
            for (int p = 0; p < 8; p++)
            {
                itog[p] = new int[8];
                for (int q = 0; q < 8; q++)
                {
                    itog[p][q] = 0;
                }
            }
            for (int p = 0; p < 8; p++)
            {
                int tek = 0;
                do
                {
                    tek = rnd.Next(0, 8);
                } while (isp[tek] != false);
                itog[p][tek] = 1;
                isp[tek] = true;
            }
            return itog;
        }

        static int[][] genPerestanovkaMod()
        {
            bool[] isp = new bool[10];
            for (int p = 0; p < 10; p++)
            {
                isp[p] = false;
            }
            Random rnd = new Random();
            int[][] itog = new int[10][];
            for (int p = 0; p < 10; p++)
            {
                itog[p] = new int[10];
                for (int q = 0; q < 10; q++)
                {
                    itog[p][q] = 0;
                }
            }
            for (int p = 0; p < 10; p++)
            {
                int tek = 0;
                do
                {
                    tek = rnd.Next(0, 10);
                } while (isp[tek] != false);
                itog[p][tek] = 1;
                isp[tek] = true;
            }
            return itog;
        }

        static int genZamenaMod()
        {
            Random rnd = new Random();
            return rnd.Next(1, 255);
        }

        public static void AppendAllBytes(string path, byte[] bytes)
        {
            using (var stream = new FileStream(path, FileMode.Append))
            {
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        static void writeKey(string GammaIsh, int[][] GammaMod, int[][] PerIsh, int[][] PerMod, int ZamenaIsh, int ZamenaMod)
        {
            Console.Write("Введите имя файла ключа: ");
            string m1 = Console.ReadLine();

            File.WriteAllText(m1, "");
            byte[] bytes = new byte[GammaIsh.Length];
            for(int p = 0; p < GammaIsh.Length; p++)
            {
                bytes[p] = (byte)GammaIsh[p];
            }
            AppendAllBytes(m1, bytes);
            string temp = "";
            int tek = 0;
            for (int p = 0; p < 8; p++)
            {
                tek = 0;
                for (int q = 0; q < 8; q++)
                {
                    tek += GammaMod[p][q] * (int)Math.Pow(2, 7 - q);
                }
                temp += (char)tek;
            }
            bytes = new byte[temp.Length];
            for (int p = 0; p < temp.Length; p++)
            {
                bytes[p] = (byte)temp[p];
            }
            AppendAllBytes(m1, bytes);

            temp = "0123456789AB";
            bytes = new byte[temp.Length];
            for (int p = 0; p < temp.Length; p++)
            {
                bytes[p] = (byte)temp[p];
            }
            AppendAllBytes(m1, bytes);
            temp = "";

            tek = 0;
            int pos = 0;
            for (int p = 0; p < 10; p++)
            {              
                for (int q = 0; q < 10; q++)
                {
                    tek += PerIsh[p][q] * (int)Math.Pow(2, 7 - pos);
                    pos++;
                    if(pos == 8)
                    {
                        pos = 0;
                        temp+= (char)tek;
                        tek = 0;
                    }
                }
            }
            if(pos != 0) temp += (char)tek;
            bytes = new byte[temp.Length];
            for (int p = 0; p < temp.Length; p++)
            {
                bytes[p] = (byte)temp[p];
            }
            AppendAllBytes(m1, bytes);
            temp = "";
            tek = 0;
            pos = 0;
            for (int p = 0; p < 10; p++)
            {
                for (int q = 0; q < 10; q++)
                {
                    tek += PerMod[p][q] * (int)Math.Pow(2, 7 - pos);
                    pos++;
                    if (pos == 8)
                    {
                        pos = 0;
                        temp += (char)tek;
                        tek = 0;
                    }
                }
            }
            if (pos != 0) temp += (char)tek;
            
            bytes = new byte[temp.Length];
            for (int p = 0; p < temp.Length; p++)
            {
                bytes[p] = (byte)temp[p];
            }
            AppendAllBytes(m1, bytes);
            temp = "";
            temp += (char)ZamenaIsh;
            temp += (char)ZamenaMod;
            bytes = new byte[temp.Length];
            for (int p = 0; p < temp.Length; p++)
            {
                bytes[p] = (byte)temp[p];
            }
            AppendAllBytes(m1, bytes);
            Console.Clear();
            Console.WriteLine("Ключ успешно сгенерирован");

            Console.WriteLine();
        }

        static void Encode_Decode(int round, bool state)
        {
            Console.Write("Введите имя файла ключа: ");
            string m1 = Console.ReadLine();
            if (!File.Exists(m1))
            {
                Console.WriteLine("Файл не существует");
                Console.WriteLine();
                return;
            }
            byte[] bytes = File.ReadAllBytes(m1);
            string shifr = "";
            for(int p = 0; p < bytes.Length; p++)
            {
                shifr += (char)bytes[p];
            }
            if(shifr.Length != 64)
            {
                Console.WriteLine("Неверная длина ключа");
                Console.WriteLine();
                return;
            }

            string GammaIsh = shifr.Substring(0, 16);
            shifr = shifr.Substring(16);

            int[][] GammaMod = new int[8][];            
            for (int p = 0; p < 8; p++)
            {
                GammaMod[p] = new int[8];
                int tek = (int)shifr[p];
                for(int q = 0; q < 8; q++)
                {
                    if (tek % Math.Pow(2, 8 - q) / Math.Pow(2, 7 - q) == 1) GammaMod[p][q] = 1;
                    else GammaMod[p][q] = 0;
                }
            }
            bool Error = false;
            for(int p = 0; p < 8; p++)
            {
                bool tekStr = false;
                for(int q = 0; q < 8; q++)
                {
                    if (GammaMod[p][q] == 1 && tekStr == false) tekStr = true;
                    else if(GammaMod[p][q] == 1 && tekStr == true)
                    {
                        Error = true;
                        break;
                    }
                }
                if (Error == true) break;
            }
            if(Error == true)
            {
                Console.WriteLine("Ошибка при чтении матрицы перестановки для шифра Гаммирования");
                Console.WriteLine();
                return;
            }
            shifr = shifr.Substring(8);

            if(shifr.Substring(0,12) != "0123456789AB")
            {
                Console.WriteLine("Ключ был подвергнут изменению константы");
                Console.WriteLine();
                return;
            }
            shifr = shifr.Substring(12);

            int[][] PerIsh = new int[10][];
            int tekChar = 0;
            int tekPos = 0;
            for(int p = 0; p < 10; p++)
            {
                PerIsh[p] = new int[10];
                for(int q = 0; q < 10; q++)
                {
                    int tek = (int)shifr[tekChar];
                    if (tek % Math.Pow(2, 8 - tekPos) / Math.Pow(2, 7 - tekPos) == 1) PerIsh[p][q] = 1;
                    else PerIsh[p][q] = 0;
                    tekPos++;
                    if(tekPos == 8)
                    {
                        tekPos = 0;
                        tekChar++;
                    }
                }
            }
            Error = false;
            for (int p = 0; p < 10; p++)
            {
                bool tekStr = false;
                for (int q = 0; q < 10; q++)
                {
                    if (PerIsh[p][q] == 1 && tekStr == false) tekStr = true;
                    else if (PerIsh[p][q] == 1 && tekStr == true)
                    {
                        Error = true;
                        break;
                    }
                }
                if (Error == true) break;
            }
            if (Error == true)
            {
                Console.WriteLine("Ошибка при чтении исходной матрицы для шифра Перестановки");
                Console.WriteLine();
                return;
            }
            shifr = shifr.Substring(13);

            int[][] PerMod = new int[10][];
            tekChar = 0;
            tekPos = 0;
            for (int p = 0; p < 10; p++)
            {
                PerMod[p] = new int[10];
                for (int q = 0; q < 10; q++)
                {
                    int tek = (int)shifr[tekChar];
                    if (tek % Math.Pow(2, 8 - tekPos) / Math.Pow(2, 7 - tekPos) == 1) PerMod[p][q] = 1;
                    else PerMod[p][q] = 0;
                    tekPos++;
                    if (tekPos == 8)
                    {
                        tekPos = 0;
                        tekChar++;
                    }
                }
            }
            Error = false;
            for (int p = 0; p < 10; p++)
            {
                bool tekStr = false;
                for (int q = 0; q < 10; q++)
                {
                    if (PerMod[p][q] == 1 && tekStr == false) tekStr = true;
                    else if (PerMod[p][q] == 1 && tekStr == true)
                    {
                        Error = true;
                        break;
                    }
                }
                if (Error == true) break;
            }
            if (Error == true)
            {
                Console.WriteLine("Ошибка при чтении перестановочной матрицы для шифра Перестановки");
                Console.WriteLine();
                return;
            }           
            shifr = shifr.Substring(13);
            int ZamenaIsh = (int)shifr[0];
            int ZamenaMod = (int)shifr[1];

            bool[] isp = new bool[10];
            for(int p = 0; p < 10; p++)
            {
                isp[p] = false;
            }
            int tempo = -1;
            for(int p = 0; p < 10; p++)
            {
                bool find = false;
                for(int q = 0; q < 10; q++)
                {
                    if(PerIsh[p][q] == 1)
                    {
                        isp[q] = true;
                        find = true;
                        break;
                    }
                }
                if (find == false) tempo = p;
            }
            if(tempo != -1)
            {
                for(int p = 0; p < 10; p++)
                {
                    if(isp[p] == false)
                    {
                        PerIsh[tempo][p] = 1;
                    }
                }
            }

            for (int p = 0; p < 10; p++)
            {
                isp[p] = false;
            }
            tempo = -1;
            for (int p = 0; p < 10; p++)
            {
                bool find = false;
                for (int q = 0; q < 10; q++)
                {
                    if (PerMod[p][q] == 1)
                    {
                        isp[q] = true;
                        find = true;
                        break;
                    }
                }
                if (find == false) tempo = p;
            }
            if (tempo != -1)
            {
                for (int p = 0; p < 10; p++)
                {
                    if (isp[p] == false)
                    {
                        PerMod[tempo][p] = 1;
                    }
                }
            }
            Console.Write("Введите имя исходного файла: ");
            m1 = Console.ReadLine();
            if (!File.Exists(m1))
            {
                Console.WriteLine("Файл не существует");
                Console.WriteLine();
                return;
            }
            Console.Write("Введите имя итогового файла: ");
            string m2 = Console.ReadLine();

            if (state == true)
            {
                byte[] bytes2 = File.ReadAllBytes(m1);
                string message = "";
                for (int p = 0; p < bytes2.Length; p++)
                {
                    message += (char)bytes2[p];
                }
                byte[] hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(message));
                int count = 0;
                while (message.Length % 800 != 0)
                {
                    message += " ";
                    count++;
                }
                for (int p = 0; p < round; p++)
                {
                    Console.WriteLine("Итерация " + (p+1));
                    message = Encode_Perestanovka(message, getKeyPerestanovka(PerIsh, PerMod, p));
                    message = Encode_Zamena(message, getKeyZamena(ZamenaIsh, ZamenaMod, p));
                    message = Gamma(message, getKeyGamma(GammaIsh, GammaMod, p));
                }
                bytes2 = new byte[message.Length + 20];
                File.WriteAllText(m2, "");
                for(int p = 0; p < 16; p++)
                {
                    bytes2[p] = hash[p];
                }
                bytes2[16] = (byte)(count / 16777216);
                bytes2[17] = (byte)(count % 16777216 / 65536);
                bytes2[18] = (byte)(count % 65536 / 256);
                bytes2[19] = (byte)(count % 256);
                for (int p = 20; p < message.Length + 20; p++)
                {
                    bytes2[p] = (byte)message[p - 20];
                }
                AppendAllBytes(m2, bytes2);
                Console.WriteLine("Файл зашифрован");
                Console.WriteLine();
            }
            else
            {
                byte[] bytes2 = File.ReadAllBytes(m1);
                string message = "";
                for (int p = 20; p < bytes2.Length; p++)
                {
                    message += (char)bytes2[p];
                }
                byte[] hash = new byte[16];
                for(int p = 0; p < 16; p++)
                {
                    hash[p] = bytes2[p];
                }
                int count = bytes2[16] * 16777216 + bytes2[17] * 65536 + bytes2[18] * 256 + bytes2[19];
                for (int p = round - 1; p >= 0; p--)
                {
                    Console.WriteLine("Итерация " + (round - p));
                    message = Gamma(message, getKeyGamma(GammaIsh, GammaMod, p));
                    message = Decode_Zamena(message, getKeyZamena(ZamenaIsh, ZamenaMod, p));
                    message = Decode_Perestanovka(message, getKeyPerestanovka(PerIsh, PerMod, p));
                }
                if (count != 0)
                {
                    message = message.Substring(0, message.Length - count);
                }
                bytes2 = new byte[message.Length];
                for (int p = 0; p < message.Length; p++)
                {
                    bytes2[p] = (byte)message[p];
                }
                File.WriteAllText(m2, "");
                AppendAllBytes(m2, bytes2);
                byte[] hash2 = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(message));
                bool good = true;
                for(int p = 0; p < 16; p++)
                {
                    if(hash[p] != hash2[p])
                    {
                        good = false;
                        break;
                    }
                }
                if (good)
                {
                    Console.WriteLine("Файл расшифрован успешно");
                }
                else
                {
                    Console.WriteLine("Файл расшифрован с ошибкой");
                }
                
                Console.WriteLine();
            }
            
        }

        static void Main(string[] args) //В контейнер размер количество добавленных байт
        {
            while(true)
            {
                Console.WriteLine("Программа итеративного шифрования и дешифрования");
                Console.WriteLine("Предлагаемые действия:");
                Console.WriteLine();
                Console.WriteLine("1) Сгенерировать ключ");
                Console.WriteLine("2) Зашифровать файл");
                Console.WriteLine("3) Расшифровать файл");
                Console.WriteLine("4) Выйти из программы");
                Console.WriteLine();
                Console.Write("Ваше действие: ");
                int choice = 4;
                int.TryParse(Console.ReadLine(), out choice);

                if(choice == 1)
                {
                    string GammaIsh = genGammaIsh();
                    int[][] GammaMod = genGammaMod();
                    int[][] PerIsh = genPerestanovkaIsh();
                    int[][] PerMod = genPerestanovkaMod();
                    int ZamenaIsh = genZamenaIsh();
                    int ZamenaMod = genZamenaMod();
                    writeKey(GammaIsh, GammaMod, PerIsh, PerMod, ZamenaIsh, ZamenaMod);
                }
                else if(choice == 2)
                {
                    Console.Clear();
                    Console.Write("Введите количество итераций шифрования: ");
                    int kolvo = 0;
                    int.TryParse(Console.ReadLine(), out kolvo);
                    if(kolvo <= 0)
                    {
                        Console.WriteLine("Ошибка ввода количества итераций");
                        Console.WriteLine();
                    }
                    else
                    {
                        Encode_Decode(kolvo, true);
                    }
                }
                else if(choice == 3)
                {
                    Console.Clear();
                    Console.Write("Введите количество итераций шифрования: ");
                    int kolvo = 0;
                    int.TryParse(Console.ReadLine(), out kolvo);
                    if (kolvo <= 0)
                    {
                        Console.WriteLine("Ошибка ввода количества итераций");
                        Console.WriteLine();
                    }
                    else
                    {
                        Encode_Decode(kolvo, false);
                    }
                }
                else
                {
                    break;
                }
            }

        }
    }
}
