﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Numerics;

namespace CA
{
    class Program
    {
        public static Socket talk;

        static void Say(int message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static void Say(string message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static int ListenInt()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            int tek;
            int.TryParse(message, out tek);
            return tek;
        }

        static uint ListenUint()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            uint tek;
            uint.TryParse(message, out tek);
            return tek;
        }

        static string ListenString()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            return message;
        }

        static void Main(string[] args)
        {
            
            IPAddress ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11111);
            Socket talking = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            talking.Bind(ipEndPoint);
            talking.Listen(1000);
            
            while (true)
            {
                Console.WriteLine("Я CA. Жду подключений на порту 11111");
                talk = talking.Accept();
                
                byte Version_number = (byte)ListenInt();
                byte Serial_number = (byte)ListenInt();
                string Subject_name = ListenString();
                BigInteger Subject_public_key;
                BigInteger.TryParse(ListenString(), out Subject_public_key);
                uint Validity_period = ListenUint();

                Console.WriteLine();
                Console.WriteLine("Получил следующую информацию:");
                Console.WriteLine("Version_number: " + Version_number.ToString());
                Console.WriteLine("Serial_number: " + Serial_number.ToString());
                Console.WriteLine("Subject_name: " + Subject_name);
                Console.WriteLine("Subject_public_key: " + Subject_public_key.ToString());
                Console.WriteLine("Validity_period: " + Validity_period.ToString());
                byte[] hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(Version_number.ToString() + Serial_number.ToString() + Subject_name + Subject_public_key.ToString() + Validity_period.ToString()));

                Console.WriteLine();
                Console.WriteLine("Сгенерировал следующую подпись: " + new BigInteger(hash).ToString());
                Say(new BigInteger(hash).ToString());
                Console.WriteLine();
                talk.Shutdown(SocketShutdown.Both);
            }
            Console.Read();
        }
    }
}
