﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Numerics;
using System.Globalization;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Server
{
    class Program
    {
        private static BigInteger exp = 65537;
        private static int MillerRabinCount = 100;
        private static byte[] writeKey;
        private static byte[] readKey;

        static BigInteger getRandom(int length)
        {
            byte[] data = new byte[length];
            while (true)
            {
                Random random = new Random((int)DateTime.Now.Ticks);
                random.NextBytes(data);
                if (new BigInteger(data) > 1) break;
            }
            return new BigInteger(data);
        }

        private static bool Witness(BigInteger a, BigInteger n)
        {

            BigInteger d = n - 1;
            int s = 0;
            while (d % 2 == 0)
            {
                s++;
                d /= 2;
            }

            if (BigInteger.ModPow(a, d, n) == 1) return true;

            for (int p = 0; p <= s - 1; p++)
            {
                if (BigInteger.ModPow(a, BigInteger.Pow(2, p) * d, n) == n - 1) return true;
            }
            return false;
        }

        public static bool IsPrimeMillerRabin(BigInteger n, int s, int length)
        {
            if (n % 2 == 0 || n <= 1) return false;
            for (int j = 0; j < s; j++)
            {
                BigInteger a = 0;
                while (true)
                {
                    a = getRandom(length);
                    if (a < n) break;
                }
                if (!Witness(a, n))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool PseudoPrime(BigInteger n)
        {
            BigInteger modularExponentiation = ModularExponentiation(2, BigInteger.Subtract(n, 1), n);
            if (!modularExponentiation.IsOne)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static BigInteger ModularExponentiation(BigInteger a, BigInteger b, BigInteger n)
        {
            return BigInteger.ModPow(a, b, n);
        }

        private static BigInteger GCD(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }
            BigInteger x1, y1;
            BigInteger d = GCD(b % a, a, out x1, out y1);
            x = y1 - (b / a) * x1;
            y = x1;
            return d;
        }
        private static BigInteger Foo(BigInteger a, BigInteger m)
        {
            BigInteger x, y;
            BigInteger g = GCD(a, m, out x, out y);
            if (g != 1)
                throw new ArgumentException();
            return (x % m + m) % m;
        }

        public static Socket talk;

        static void Say(int message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static void Say(uint message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static void Say(string message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static int ListenInt()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            int tek;
            int.TryParse(message, out tek);
            return tek;
        }

        static string ListenString()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            return message;
        }

        static uint ListenUint()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            uint tek;
            uint.TryParse(message, out tek);
            return tek;
        }

        static string EncString(string message)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            for(int p = 0; p < data.Length; p++)
            {
                data[p] ^= writeKey[p % writeKey.Length];
            }
            string itog = "";
            for (int p = 0; p < data.Length; p++)
            {
                itog += (char)data[p];
            }
            return itog;
        }

        static string DecString(string message)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);

            byte[] itog = new byte[0];
            for (int p = 0; p < data.Length; p++)
            {
                if (data[p] >= 194)
                {
                    p++;
                    byte[] temp = new byte[itog.Length + 1];
                    Array.Copy(itog, temp, itog.Length);
                    temp[itog.Length] = (byte)(data[p] + (data[p - 1] - 194) * 64);
                    itog = temp;
                }
                else
                {
                    byte[] temp = new byte[itog.Length + 1];
                    Array.Copy(itog, temp, itog.Length);
                    temp[itog.Length] = data[p];
                    itog = temp;
                }
            }
            for (int p = 0; p < itog.Length; p++)
            {
                itog[p] ^= readKey[p % readKey.Length];

            }
            return Encoding.UTF8.GetString(itog);
        }

        static byte[] EncByte(byte[] message)
        {
            byte[] data = message;
            for (int p = 0; p < data.Length; p++)
            {
                data[p] ^= writeKey[p % writeKey.Length];
            }
            return data;
        }

        static byte[] DecByte(byte[] message)
        {
            byte[] data = message;
            for (int p = 0; p < data.Length; p++)
            {
                data[p] ^= readKey[p % readKey.Length];
            }
            return data;
        }

        static int EncInt(int message)
        {
            return message ^ writeKey[0];
        }

        static int DecInt(int message)
        {
            return message ^ readKey[0];
        }

        static uint DecUint(uint message)
        {
            return message ^ readKey[0];
        }

        public struct Certificate
        {
            public byte Version_number;
            public byte Serial_number;
            public string Subject_name;
            public byte[] Subject_public_key;
            public uint Validity_period;
            public byte[] Signature;

            public Certificate(byte a, byte b, string c, byte[] d, uint e, byte[] f)
            {
                Version_number = a;
                Serial_number = b;
                Subject_name = c;
                Subject_public_key = d;
                Validity_period = e;
                Signature = f;
            }
        };

        static void Main(string[] args)
        {
            Console.WriteLine("Я сервер. Жду клиента на порту 11110");
            IPAddress ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11110);
            Socket talking = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            talking.Bind(ipEndPoint);
            talking.Listen(1000);
            talk = talking.Accept();
            string message;

            //Client Hello
            while (true) 
            {
                message = ListenString();
                if (message == "CLIENT-HELLO") break;
            }
            BigInteger sessionID;
            BigInteger.TryParse(ListenString(), out sessionID);
            string encClient = ListenString();
            string hashClient = ListenString();
            Console.WriteLine();
            Console.WriteLine("Получил:");
            Console.WriteLine("Challenge_data: " + message);
            Console.WriteLine("Session_id: " + sessionID);
            Console.WriteLine("Cipher_specs_cipher: " + encClient);
            Console.WriteLine("Cipher_specs_hash: " + hashClient);

            //Server Hello
            Console.WriteLine();
            Console.Write("Использовать CA?(y/n): ");
            message = Console.ReadLine();
            BigInteger n;
            BigInteger d;
            Certificate cert;
            Console.WriteLine();
            Console.WriteLine("Генерирую ключ");
            BigInteger p;
            BigInteger q;
            while (true)
            {
                while (true)
                {
                    p = getRandom(32);
                    bool good = true;
                    for (int r = 2; r < 10000; r++)
                    {
                        if (p % r == 0)
                        {
                            good = false;
                            break;
                        }
                    }
                    if (good == false) continue;
                    if (PseudoPrime(p) && IsPrimeMillerRabin(p, MillerRabinCount, 16)) break;
                }
                while (true)
                {
                    q = getRandom(32);
                    bool good = true;
                    for (int r = 2; r < 10000; r++)
                    {
                        if (q % r == 0)
                        {
                            good = false;
                            break;
                        }
                    }
                    if (good == false) continue;
                    if (PseudoPrime(q) && IsPrimeMillerRabin(q, MillerRabinCount, 16)) break;
                }
                n = p * q;
                if (n.ToByteArray().Length != 64) continue;
                BigInteger eiler = (p - 1) * (q - 1);
                d = Foo(exp, eiler);
                break;
            }
            DateTime now = DateTime.Now;
            uint time;
            uint.TryParse(now.Subtract(new DateTime(now.Year, 1, 1)).Days.ToString(), out time);
            time = (uint)now.Minute + (uint)now.Hour * 60 + time * 1440;
            time = time * 65536 + time + 60;
            if (message.Length != 1 || (message[0] != 'Y' && message[0] != 'y'))
            {               
                cert = new Certificate(1, 1, "SSL-Server      ", n.ToByteArray(), time, MD5.Create().ComputeHash(Encoding.UTF8.GetBytes("11SSL-Server      " + n.ToString() + time.ToString())));
            }
            else
            {
                Socket save = talk;
                ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
                ipEndPoint = new IPEndPoint(ipAddr, 11111);
                talk = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                talk.Connect(ipEndPoint);
                Say(1);
                Say(1);
                Say("SSL-Server      ");
                Say(n.ToString());
                Say(time);

                BigInteger unparsedHash;
                BigInteger.TryParse(ListenString(), out unparsedHash);
                byte[] hash = unparsedHash.ToByteArray();
                cert = new Certificate(1, 1, "SSL-Server      ", n.ToByteArray(), time, hash);
                talk = save;
                Console.WriteLine();
                Console.WriteLine("Получена подпись от CA");
            }
            BigInteger connection_id = getRandom(16);
            Say("SERVER-HELLO");
            Say(connection_id.ToString());

            Say(cert.Version_number);
            Say(cert.Serial_number);
            Say(cert.Subject_name);
            Say(new BigInteger(cert.Subject_public_key).ToString());
            Say(cert.Validity_period);
            Say(new BigInteger(cert.Signature).ToString());


            Console.WriteLine();
            Console.WriteLine("Отправил:");
            Console.WriteLine("Server_hello: SERVER-HELLO");
            Console.WriteLine("Connection_id: " + connection_id.ToString());
            Console.WriteLine();
            Console.WriteLine("Получен и отправлен следующий сертификат:");
            Console.WriteLine("Version_number: " + cert.Version_number.ToString());
            Console.WriteLine("Serial_number: " + cert.Serial_number.ToString());
            Console.WriteLine("Subject_name: " + cert.Subject_name);
            Console.WriteLine("Subject_public_key: " + new BigInteger(cert.Subject_public_key).ToString());
            Console.WriteLine("Validity_period: " + cert.Validity_period.ToString());
            Console.WriteLine("Signature: " + new BigInteger(cert.Signature).ToString());

            //Client Master Key
            BigInteger cipherBig;
            BigInteger.TryParse(ListenString(), out cipherBig);
            Console.WriteLine();
            Console.WriteLine("Получен зашифрованный мастер-ключ: " + cipherBig.ToString());
            byte[] masterKey;
            byte[] cipherMaster;
            if (cipherBig.ToByteArray().Length > 200)
            {
                masterKey = new byte[256];
                cipherMaster = new byte[320];
            }
            else
            {
                masterKey = new byte[128];
                cipherMaster = new byte[192];
            }
            for(int r = 0; r < cipherMaster.Length; r++)
            {
                cipherMaster[r] = 0;
            }
            Array.Copy(cipherBig.ToByteArray(), cipherMaster, cipherBig.ToByteArray().Length);
           
            for (int r = 0; r < cipherMaster.Length / 64; r++)
            {
                byte[] encodePart = new byte[65];

                Array.Copy(cipherMaster, r * 64, encodePart, 0, 64);
                BigInteger bigEnc = new BigInteger(encodePart);
                bigEnc = BigInteger.ModPow(bigEnc, d, n);
                if (r != cipherMaster.Length / 64 - 1)
                {
                    Array.Copy(bigEnc.ToByteArray(), 0, masterKey, r * 63, 63);
                }
                else
                {
                    if(masterKey.Length == 128)
                    {
                        Array.Copy(bigEnc.ToByteArray(), 0, masterKey, r * 63, 2);
                    }
                    else
                    {
                        Array.Copy(bigEnc.ToByteArray(), 0, masterKey, r * 63, 4);
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine("Расшифрован мастер-ключ: " + new BigInteger(masterKey).ToString());
            writeKey = new byte[masterKey.Length / 2];
            Array.Copy(masterKey, 0, writeKey, 0, writeKey.Length);
            readKey = new byte[masterKey.Length / 2];
            Array.Copy(masterKey, masterKey.Length / 2, readKey, 0, readKey.Length);

            //Server Verify
            Say(EncString("SERVER-VERIFY"));
            Say(EncString("CLIENT-HELLO"));
            Say(Encoding.UTF8.GetString(EncByte(sessionID.ToByteArray())));
            Console.WriteLine();
            Console.WriteLine("Отправил зашифрованный Server-verify");

            //Request Sertificate
            Say(EncString("REQUEST-SERTIFICATE"));
            Say(EncString("RSA"));
            Say(EncString("MD5"));
            Say(EncString("Challenge_data"));
            Console.WriteLine();
            Console.WriteLine("Отправил:");
            Console.WriteLine("REQUEST-SERTIFICATE: RSA, MD5");
            Console.WriteLine("Certificate challenge data: Challenge_data");
            Console.WriteLine();

            //Client Sertificate
            message = DecString(ListenString());
            Console.WriteLine("Получил:");
            Console.WriteLine(message);
            message = DecString(ListenString());
            Console.WriteLine(message);
            Certificate cert2;
            int a = DecInt(ListenInt());
            int b = DecInt(ListenInt());
            string c = DecString(ListenString());
            BigInteger d2;
            BigInteger.TryParse(DecString(ListenString()), out d2);
            uint e = DecUint(ListenUint());
            BigInteger f;
            BigInteger.TryParse(DecString(ListenString()), out f);
            cert2 = new Certificate((byte)a, (byte)b, c, d2.ToByteArray(), e, f.ToByteArray());
            Console.WriteLine();
            Console.WriteLine("Получил следующий сертификат:");
            Console.WriteLine("Version_number: " + cert2.Version_number.ToString());
            Console.WriteLine("Serial_number: " + cert2.Serial_number.ToString());
            Console.WriteLine("Subject_name: " + cert2.Subject_name);
            Console.WriteLine("Subject_public_key: " + new BigInteger(cert2.Subject_public_key).ToString());
            Console.WriteLine("Validity_period: " + cert2.Validity_period.ToString());
            Console.WriteLine("Signature: " + new BigInteger(cert2.Signature).ToString());
            message = DecString(ListenString());
            Console.WriteLine();
            Console.WriteLine("Получил подписи:");
            Console.WriteLine(message);
            message = DecString(ListenString());
            Console.WriteLine(message);

            //Server Finished
            Say(EncString("SERVER-FINISHED"));
            Say(EncString(sessionID.ToString()));
            Console.WriteLine();
            Console.WriteLine("Отправил:");
            Console.WriteLine("SERVER-FINISHED");
            Console.WriteLine(sessionID.ToString());

            //Client Finished
            Console.WriteLine();
            message = DecString(ListenString());
            Console.WriteLine("Получил:");
            Console.WriteLine(message);
            message = DecString(ListenString());
            Console.WriteLine(message);

            Console.WriteLine();
            Console.WriteLine("Можем приступать к обмену сообщениями");
            talk.Shutdown(SocketShutdown.Both);
            Console.Read();
        }
    }
}
