﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;
using System.Numerics;
using System.Security.Cryptography;

namespace Client
{
    class Program
    {
        public static Socket talk;
        public static BigInteger exp = 65537;
        private static int MillerRabinCount = 100;
        private static byte[] writeKey;
        private static byte[] readKey;

        private static bool Witness(BigInteger a, BigInteger n)
        {

            BigInteger d = n - 1;
            int s = 0;
            while (d % 2 == 0)
            {
                s++;
                d /= 2;
            }

            if (BigInteger.ModPow(a, d, n) == 1) return true;

            for (int p = 0; p <= s - 1; p++)
            {
                if (BigInteger.ModPow(a, BigInteger.Pow(2, p) * d, n) == n - 1) return true;
            }
            return false;
        }

        public static bool IsPrimeMillerRabin(BigInteger n, int s, int length)
        {
            if (n % 2 == 0 || n <= 1) return false;
            for (int j = 0; j < s; j++)
            {
                BigInteger a = 0;
                while (true)
                {
                    a = getRandom(length);
                    if (a < n) break;
                }
                if (!Witness(a, n))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool PseudoPrime(BigInteger n)
        {
            BigInteger modularExponentiation = ModularExponentiation(2, BigInteger.Subtract(n, 1), n);
            if (!modularExponentiation.IsOne)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static BigInteger ModularExponentiation(BigInteger a, BigInteger b, BigInteger n)
        {
            return BigInteger.ModPow(a, b, n);
        }

        private static BigInteger GCD(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }
            BigInteger x1, y1;
            BigInteger d = GCD(b % a, a, out x1, out y1);
            x = y1 - (b / a) * x1;
            y = x1;
            return d;
        }
        private static BigInteger Foo(BigInteger a, BigInteger m)
        {
            BigInteger x, y;
            BigInteger g = GCD(a, m, out x, out y);
            if (g != 1)
                throw new ArgumentException();
            return (x % m + m) % m;
        }

        static BigInteger getRandom(int length)
        {
            byte[] data = new byte[length];
            while (true)
            {
                Random random = new Random((int)DateTime.Now.Ticks);
                random.NextBytes(data);
                if (new BigInteger(data) > 1) break;
            }
            return new BigInteger(data);
        }

        static void Say(int message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static void Say(string message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static void Say(uint message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static int ListenInt()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            int tek;
            int.TryParse(message, out tek);
            return tek;
        }

        static string ListenString()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            return message;
        }

        static uint ListenUint()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            uint tek;
            uint.TryParse(message, out tek);
            return tek;
        }

        static string EncString(string message)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            for (int p = 0; p < data.Length; p++)
            {
                data[p] ^= writeKey[p % writeKey.Length];
            }
            string itog = "";
            for (int p = 0; p < data.Length; p++)
            {
                itog += (char)data[p];
            }
            return itog;
        }

        static string DecString(string message)
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            
            byte[] itog = new byte[0];
            for (int p = 0; p < data.Length; p++)
            {
                if (data[p] >= 194)
                {
                    p++;
                    byte[] temp = new byte[itog.Length + 1];
                    Array.Copy(itog, temp, itog.Length);
                    temp[itog.Length] = (byte)(data[p] + (data[p - 1] - 194) * 64);
                    itog = temp;
                }
                else
                {
                    byte[] temp = new byte[itog.Length + 1];
                    Array.Copy(itog, temp, itog.Length);
                    temp[itog.Length] = data[p];
                    itog = temp;
                }
            }
            for(int p = 0; p < itog.Length; p++)
            {
                itog[p] ^= readKey[p % readKey.Length];
                
            }
            return Encoding.UTF8.GetString(itog);
        }

        static byte[] EncByte(byte[] message)
        {
            byte[] data = message;
            for (int p = 0; p < data.Length; p++)
            {
                data[p] ^= writeKey[p % writeKey.Length];
            }
            return data;
        }

        static byte[] DecByte(byte[] message)
        {
            byte[] data = message;
            for (int p = 0; p < data.Length; p++)
            {
                data[p] ^= readKey[p % readKey.Length];
            }
            return data;
        }

        static int EncInt(int message)
        {
            return message ^ writeKey[0];
        }

        static int DecInt(int message)
        {
            return message ^ readKey[0];
        }

        public struct Certificate
        {
            public byte Version_number;
            public byte Serial_number;
            public string Subject_name;
            public byte[] Subject_public_key;
            public uint Validity_period;
            public byte[] Signature;

            public Certificate(byte a, byte b, string c, byte[] d, uint e, byte[] f)
            {
                Version_number = a;
                Serial_number = b;
                Subject_name = c;
                Subject_public_key = d;
                Validity_period = e;
                Signature = f;
            }
        };

        static void Main(string[] args)
        {
            Console.WriteLine("Я клиент. Подключаюсь к серверу по порту 11110");
            IPAddress ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11110);
            talk = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            talk.Connect(ipEndPoint);

            //Client Hello
            BigInteger sessionID = getRandom(16);
            Say("CLIENT-HELLO"); 
            Say(sessionID.ToString());
            Say("RSA");
            Say("MD5");
            Console.WriteLine();
            Console.WriteLine("Отправил:");
            Console.WriteLine("Challenge_data: CLIENT-HELLO");
            Console.WriteLine("Session_id: " + sessionID.ToString());
            Console.WriteLine("Cipher_specs_cipher: RSA");
            Console.WriteLine("Cipher_specs_hash: MD5");

            //Server Hello
            string message = ListenString();
            BigInteger connection_id;
            BigInteger.TryParse(ListenString(), out connection_id);

            Certificate cert;
            int a = ListenInt();
            int b = ListenInt();
            string c = ListenString();
            BigInteger d;
            BigInteger.TryParse(ListenString(), out d);
            uint e = ListenUint();
            BigInteger f;
            BigInteger.TryParse(ListenString(), out f);
            cert = new Certificate((byte)a, (byte)b, c, d.ToByteArray(), e, f.ToByteArray());

            Console.WriteLine();
            Console.WriteLine("Получил:");
            Console.WriteLine("Server_hello: " + message);
            Console.WriteLine("Connection_id: " + connection_id.ToString());
            Console.WriteLine();
            Console.WriteLine("Получил следующий сертификат:");
            Console.WriteLine("Version_number: " + cert.Version_number.ToString());
            Console.WriteLine("Serial_number: " + cert.Serial_number.ToString());
            Console.WriteLine("Subject_name: " + cert.Subject_name);
            Console.WriteLine("Subject_public_key: " + new BigInteger(cert.Subject_public_key).ToString());
            Console.WriteLine("Validity_period: " + cert.Validity_period.ToString());
            Console.WriteLine("Signature: " + new BigInteger(cert.Signature).ToString());

            //Client Master Key
            byte[] hash1 = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(cert.Version_number.ToString()));
            byte[] hash2 = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(cert.Serial_number.ToString()));
            byte[] hash3 = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(cert.Subject_name));
            byte[] hash4 = MD5.Create().ComputeHash(cert.Subject_public_key);
            byte[] hash5 = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(cert.Validity_period.ToString()));
            byte[] hash6 = MD5.Create().ComputeHash(cert.Signature);
            byte[] masterKey;
            Console.WriteLine();
            Console.Write("Использовать мастер-ключ длинной 1024 бит(y) или 2048(n)?: ");
            message = Console.ReadLine();
            if (message.Length != 1 || (message[0] != 'Y' && message[0] != 'y'))
            {
                masterKey = new byte[256];
                Array.Copy(hash1, 0, masterKey, 0, 16);
                Array.Copy(hash2, 0, masterKey, 16, 16);
                Array.Copy(hash3, 0, masterKey, 32, 16);
                Array.Copy(hash4, 0, masterKey, 48, 16);
                Array.Copy(hash5, 0, masterKey, 64, 16);
                Array.Copy(hash6, 0, masterKey, 80, 16);
                Array.Copy(connection_id.ToByteArray(), 0, masterKey, 96, 16);
                Array.Copy(sessionID.ToByteArray(), 0, masterKey, 112, 16);
                Array.Copy(MD5.Create().ComputeHash(hash1), 0, masterKey, 128, 16);
                Array.Copy(MD5.Create().ComputeHash(hash2), 0, masterKey, 144, 16);
                Array.Copy(MD5.Create().ComputeHash(hash3), 0, masterKey, 160, 16);
                Array.Copy(MD5.Create().ComputeHash(hash4), 0, masterKey, 176, 16);
                Array.Copy(MD5.Create().ComputeHash(hash5), 0, masterKey, 192, 16);
                Array.Copy(MD5.Create().ComputeHash(hash6), 0, masterKey, 208, 16);
                Array.Copy(MD5.Create().ComputeHash(connection_id.ToByteArray()), 0, masterKey, 224, 16);
                Array.Copy(MD5.Create().ComputeHash(sessionID.ToByteArray()), 0, masterKey, 240, 16);
            }
            else
            {
                masterKey = new byte[128];
                Array.Copy(hash1, 0, masterKey, 0, 16);
                Array.Copy(hash2, 0, masterKey, 16, 16);
                Array.Copy(hash3, 0, masterKey, 32, 16);
                Array.Copy(hash4, 0, masterKey, 48, 16);
                Array.Copy(hash5, 0, masterKey, 64, 16);
                Array.Copy(hash6, 0, masterKey, 80, 16);
                Array.Copy(connection_id.ToByteArray(), 0, masterKey, 96, 16);
                Array.Copy(sessionID.ToByteArray(), 0, masterKey, 112, 16);
            }
            Console.WriteLine();
            Console.WriteLine("Сгенерировал мастер-ключ: " + new BigInteger(masterKey));
            Console.WriteLine();
            byte[] cipherMaster = new byte[masterKey.Length / 63 * 64 + 64];
            byte[] encodePart;
            byte[] nByte = new byte[cert.Subject_public_key.Length + 1];
            Array.Copy(cert.Subject_public_key, nByte, cert.Subject_public_key.Length);
            nByte[nByte.Length - 1] = 0;
            BigInteger n = new BigInteger(nByte);
            BigInteger bigEnc;
            for (int r = 0; r < masterKey.Length / 63; r++)
            {
                encodePart = new byte[64];
                for (int s = 0; s < 64; s++)
                {
                    encodePart[s] = 0;
                }
                
                Array.Copy(masterKey, r * 63, encodePart, 0, 63);
                bigEnc = new BigInteger(encodePart);
                bigEnc = BigInteger.ModPow(bigEnc, exp, n);

                Array.Copy(bigEnc.ToByteArray(), 0, cipherMaster, r * 64, bigEnc.ToByteArray().Length);
                if (bigEnc.ToByteArray().Length == 63) cipherMaster[(r + 1) * 64 - 1] = 0;
                
            }
            encodePart = new byte[masterKey.Length % 63 + 1];
            Array.Copy(masterKey, masterKey.Length - masterKey.Length % 63, encodePart, 0, encodePart.Length - 1);
            encodePart[encodePart.Length - 1] = 0;
            bigEnc = new BigInteger(encodePart);
            bigEnc = BigInteger.ModPow(bigEnc, exp, new BigInteger(nByte));
            Array.Copy(bigEnc.ToByteArray(), 0, cipherMaster, cipherMaster.Length - 64, bigEnc.ToByteArray().Length);
            if (bigEnc.ToByteArray().Length == 63) cipherMaster[cipherMaster.Length - 1] = 0;
            Say(new BigInteger(cipherMaster).ToString());
            Console.WriteLine("Получен и отправлен следующий зашифрованный мастер-ключ: " + new BigInteger(cipherMaster));

            readKey = new byte[masterKey.Length / 2];
            Array.Copy(masterKey, 0, readKey, 0, readKey.Length);
            writeKey = new byte[masterKey.Length / 2];
            Array.Copy(masterKey, masterKey.Length / 2, writeKey, 0, writeKey.Length);

            //Server Verify
            string serverVerify = DecString(ListenString());
            string clientHello = DecString(ListenString());
            BigInteger sess = new BigInteger(DecByte(Encoding.UTF8.GetBytes(ListenString())));
            Console.WriteLine();
            Console.WriteLine("Получил:");
            Console.WriteLine(serverVerify);
            Console.WriteLine(clientHello);
            Console.WriteLine(sessionID.ToString());
            if(serverVerify != "SERVER-VERIFY" || clientHello != "CLIENT-HELLO")
            {
                Console.WriteLine("Error!");
                Console.Read();
                talk.Shutdown(SocketShutdown.Both);
            }

            //Request Sertificate
            Console.WriteLine();
            Console.WriteLine("Получил:");
            message = DecString(ListenString());
            Console.Write(message + ": ");
            message = DecString(ListenString());
            Console.Write(message + ", ");
            message = DecString(ListenString());
            Console.WriteLine(message);
            string chall_data = DecString(ListenString());
            Console.WriteLine("Certificate challenge data: " + chall_data);

            //Client Sertificate
            Console.Write("Использовать CA?(y/n): ");
            message = Console.ReadLine();
            BigInteger n2;
            BigInteger d2;
            Certificate cert2 = new Certificate();
            Console.WriteLine();
            Console.WriteLine("Генерирую ключ");
            BigInteger p2;
            BigInteger q2;
            while (true)
            {
                while (true)
                {
                    p2 = getRandom(32);
                    bool good = true;
                    for (int r = 2; r < 10000; r++)
                    {
                        if (p2 % r == 0)
                        {
                            good = false;
                            break;
                        }
                    }
                    if (good == false) continue;
                    if (PseudoPrime(p2) && IsPrimeMillerRabin(p2, MillerRabinCount, 16)) break;
                }
                while (true)
                {
                    q2 = getRandom(32);
                    bool good = true;
                    for (int r = 2; r < 10000; r++)
                    {
                        if (q2 % r == 0)
                        {
                            good = false;
                            break;
                        }
                    }
                    if (good == false) continue;
                    if (PseudoPrime(q2) && IsPrimeMillerRabin(q2, MillerRabinCount, 16)) break;
                }
                n2 = p2 * q2;
                if (n.ToByteArray().Length != 64) continue;
                BigInteger eiler = (p2 - 1) * (q2 - 1);
                d2 = Foo(exp, eiler);
                break;
            }
            DateTime now = DateTime.Now;
            uint time;
            uint.TryParse(now.Subtract(new DateTime(now.Year, 1, 1)).Days.ToString(), out time);
            time = (uint)now.Minute + (uint)now.Hour * 60 + time * 1440;
            time = time * 65536 + time + 60;
            if (message.Length != 1 || (message[0] != 'Y' && message[0] != 'y'))
            {
                cert2 = new Certificate(1, 1, "SSL-Client      ", n2.ToByteArray(), time, MD5.Create().ComputeHash(Encoding.UTF8.GetBytes("11SSL-Client      " + n2.ToString() + time.ToString())));
            }
            else
            {
                Socket save = talk;
                ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
                ipEndPoint = new IPEndPoint(ipAddr, 11111);
                talk = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                talk.Connect(ipEndPoint);
                Say(1);
                Say(1);
                Say("SSL-Client      ");
                Say(n.ToString());
                Say(time);

                BigInteger unparsedHash;
                BigInteger.TryParse(ListenString(), out unparsedHash);
                byte[] hash = unparsedHash.ToByteArray();
                cert2 = new Certificate(1, 1, "SSL-Client      ", n2.ToByteArray(), time, hash);
                talk = save;
                Console.WriteLine();
                Console.WriteLine("Получена подпись от CA");
            }
            Say(EncString("CLIENT-SERTIFICATE"));
            Say(EncString("RSA"));
            Say(EncInt(cert2.Version_number));
            Say(EncInt(cert2.Serial_number));
            Say(EncString(cert2.Subject_name));
            Say(EncString(new BigInteger(cert2.Subject_public_key).ToString()));
            Say(EncInt((int)cert2.Validity_period));
            Say(EncString(new BigInteger(cert2.Signature).ToString()));

            Console.WriteLine();
            Console.WriteLine("Получен и отправлен следующий сертификат:");
            Console.WriteLine("Version_number: " + cert2.Version_number.ToString());
            Console.WriteLine("Serial_number: " + cert2.Serial_number.ToString());
            Console.WriteLine("Subject_name: " + cert2.Subject_name);
            Console.WriteLine("Subject_public_key: " + new BigInteger(cert2.Subject_public_key).ToString());
            Console.WriteLine("Validity_period: " + cert2.Validity_period.ToString());
            Console.WriteLine("Signature: " + new BigInteger(cert2.Signature).ToString());
            Say(EncString(new BigInteger(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(chall_data))).ToString()));
            Say(EncString(new BigInteger(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes("11" + cert.Subject_name + new BigInteger(cert.Subject_public_key).ToString() + cert.Validity_period.ToString()))).ToString()));
            Console.WriteLine();
            Console.WriteLine("Отправлены следующие подписи:");
            Console.WriteLine(new BigInteger(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(chall_data))));
            Console.WriteLine(new BigInteger(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes("11" + cert.Subject_name + new BigInteger(cert.Subject_public_key).ToString() + cert.Validity_period.ToString()))));

            //Server Finished
            Console.WriteLine();
            message = DecString(ListenString());
            Console.WriteLine("Получил:");
            Console.WriteLine(message);
            message = DecString(ListenString());
            Console.WriteLine(message);

            //Client Finished
            Say(EncString("CLIENT-FINISHED"));
            Say(EncString(connection_id.ToString()));
            Console.WriteLine();
            Console.WriteLine("Отправил:");
            Console.WriteLine("CLIENT-FINISHEDD");
            Console.WriteLine(connection_id.ToString());

            Console.WriteLine();
            Console.WriteLine("Можем приступать к обмену сообщениями");
            Console.Read();
        }
    }
}
