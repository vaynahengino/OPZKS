﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Threading.Tasks;

namespace S1
{
    class Program
    {
        public static Socket talk;
        public static int SecretLife = 120;
        public static int SeansLife = 60;
        public static int RazreshLife = 10;

        static void Say(int message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static int Listen()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            int tek;
            int.TryParse(message, out tek);
            return tek;
        }
        
        static string ListenString()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            return message;
        }

        static void Main(string[] args)
        {
            int id = 2;
            IPAddress ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11112);
            Socket talking = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            talking.Bind(ipEndPoint);
            while (true)
            {
                Console.WriteLine("IP-Address: " + Dns.GetHostByName(Dns.GetHostName()).AddressList[0]);
                Console.WriteLine("Сервер печати. Жду подключения на порту 11112");
                Console.WriteLine();
                talking.Listen(1000);
                talk = talking.Accept();
                int idc_kcs = Listen(); //Step 5
                int nc_kcs = Listen();
                int ltcsH_kcs = Listen();
                int ltcsM_kcs = Listen();
                int ltcsS_kcs = Listen();

                string[] db = File.ReadAllLines(@"D:\Laba_7\S_TGS_DB"); //Step 6
                int kstgs = -1;
                for (int p = 0; p < db.Length; p++)
                {
                    int tek;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out tek);
                    if (tek == id)
                    {
                        db[p] = db[p].Substring(db[p].IndexOf(' ') + 1);
                        int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out kstgs);
                        break;
                    }
                }

                int nc = Listen() ^ kstgs; 
                int kcs = Listen() ^ kstgs;
                int ltcsH = Listen() ^ kstgs;
                int ltcsM = Listen() ^ kstgs;
                int ltcsS = Listen() ^ kstgs;

                idc_kcs ^= kcs; 
                nc_kcs ^= kcs;
                ltcsH_kcs ^= kcs;
                ltcsM_kcs ^= kcs;
                ltcsS_kcs ^= kcs;

                string message = ListenString();
                if (nc == nc_kcs && ltcsH == ltcsH_kcs && ltcsM == ltcsM_kcs && ltcsS == ltcsS_kcs)
                {
                    Console.WriteLine("Полученная от клиента " + idc_kcs + " информация верна");
                    string decMes = "";
                    for (int p = 0; p < message.Length ; p++)
                    {
                        decMes += (char)(message[p] ^ kcs);
                    }
                    Console.WriteLine("В печать поступила следующая строка: " + decMes);
                }
                else
                {
                    Console.WriteLine("Ошибка в полученной информации");
                }
                Console.WriteLine();
            }
        }
    }
}
