﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {

        public static int SecretLife = 120;
        public static int SeansLife = 60;
        public static int RazreshLife = 10;
        public static Socket talk;

        static void Say(int message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static void Say(string message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static int Listen()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            int tek;
            int.TryParse(message, out tek);
            return tek;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("IP-Address: " + Dns.GetHostByName(Dns.GetHostName()).AddressList[0]);
            IPAddress ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
            //string IP = Console.ReadLine();
            IPHostEntry ipHost;
            IPEndPoint ipEndPoint;
            int kcs = 1;
            int kcas = 1;
            int kctgs = 1;
            int idc = 10;
            int idtgs = 1;
            int ltcsH = -1;
            int ltcsM = -1;
            int ltcsS = -1;
            int ltctgsH = -1;
            int ltctgsM = -1;
            int ltctgsS = -1;

            int idc_kastgs = 1;
            int idtgs_kastgs = 1;
            int kctgs_kastgs = 1;
            int ltctgsH_kastgs = 1;
            int ltctgsM_kastgs = 1;
            int ltctgsS_kastgs = 1;
            int nc_kastgs = 1;

            int nc_kstgs = 1;
            int kcs_kstgs = 1;
            int ltcsH_kstgs = 1;
            int ltcsM_kstgs = 1;
            int ltcsS_kstgs = 1;

            int ids = 2;
            int nc;
            while (true)
            {
                nc = 2;
                Console.Write("Я клиент " + idc + ". Сообщение для сервера печати на 11112 порту: ");
                string message = Console.ReadLine();

                DateTime now = DateTime.Now;
                int time1 = now.Hour * 3600 + now.Minute * 60 + now.Second;
                int time2 = ltcsH * 3600 + ltcsM * 60 + ltcsS;
                int time3 = ltctgsH * 3600 + ltctgsM * 60 + ltctgsS;
                int temp2 = Math.Abs(time2);
                int time1sch1 = time1;
                int time1sch2 = time1;
                if (Math.Abs(time1 - temp2) > RazreshLife) time1sch1 += 86400;
                int temp3 = Math.Abs(time3);
                if (Math.Abs(time1 - temp3) > SeansLife) time1sch2 += 86400;
                if (time2 < 0 || temp2 < time1sch1)
                {
                    if(time3 < 0 || temp3 < time1sch2) //Step 1
                    {
                        Console.WriteLine();
                        Console.WriteLine("Все тикеты устарели. Обращаюсь к AS");
                        ipEndPoint = new IPEndPoint(ipAddr, 11110);
                        talk = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                        talk.Connect(ipEndPoint);

                        Say(idc);
                        Say(idtgs);
                        Say(nc);
                        string[] db = File.ReadAllLines(@"D:\Laba_7\AS_C_DB"); //Step 2
                        kcas = -1;
                        for (int p = 0; p < db.Length; p++)
                        {
                            int tek;
                            int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out tek);
                            if (tek == idc)
                            {
                                db[p] = db[p].Substring(db[p].IndexOf(' ') + 1);
                                int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out kcas);
                                break;
                            }
                        }
                        kctgs = Listen() ^ kcas;
                        int idtgs_kcas = Listen() ^ kcas;
                        ltctgsH = Listen() ^ kcas;
                        ltctgsM = Listen() ^ kcas;
                        ltctgsS = Listen() ^ kcas;
                        Console.WriteLine("Получил время жизни ключа для связи с TGS: " + ltctgsH + " " + ltctgsM + " " + ltctgsS);
                        if (idtgs_kcas == idtgs)
                        {
                            Console.WriteLine("NC получено верно");
                        }
                        else
                        {
                            Console.WriteLine("Неверное NC");
                        }

                        idc_kastgs = Listen();
                        idtgs_kastgs = Listen();
                        kctgs_kastgs = Listen();
                        ltctgsH_kastgs = Listen();
                        ltctgsM_kastgs = Listen();
                        ltctgsS_kastgs = Listen();
                        nc_kastgs = Listen();
                        talk.Shutdown(SocketShutdown.Both);
                    }
                    Console.WriteLine(); //Step 3
                    Console.WriteLine("Обращаюсь к TGS за новым билетом");
                    ipEndPoint = new IPEndPoint(ipAddr, 11111);
                    talk = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    talk.Connect(ipEndPoint);

                    Say(idc ^ kctgs);
                    Say(ids ^ kctgs);
                    Say(ltctgsH ^ kctgs);
                    Say(ltctgsM ^ kctgs);
                    Say(ltctgsS ^ kctgs);
                    Say(nc ^ kctgs);

                    Say(idc_kastgs);
                    Say(idtgs_kastgs);
                    Say(kctgs_kastgs);
                    Say(ltctgsH_kastgs);
                    Say(ltctgsM_kastgs);
                    Say(ltctgsS_kastgs);
                    Say(nc_kastgs);

                    kcs = Listen() ^ kctgs; //Step 4
                    ltcsH = Listen() ^ kctgs;
                    ltcsM = Listen() ^ kctgs;
                    ltcsS = Listen() ^ kctgs;

                    nc_kstgs = Listen();
                    kcs_kstgs = Listen();
                    ltcsH_kstgs = Listen();
                    ltcsM_kstgs = Listen();
                    ltcsS_kstgs = Listen();

                    talk.Shutdown(SocketShutdown.Both);
                    Console.WriteLine("Получил новое разрешение");
                    Console.WriteLine();
                }
                ipEndPoint = new IPEndPoint(ipAddr, 11112); //Step 5
                talk = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                talk.Connect(ipEndPoint);

                Say(idc ^ kcs);
                Say(nc ^ kcs);
                Say(ltcsH ^ kcs);
                Say(ltcsM ^ kcs);
                Say(ltcsS ^ kcs);

                Say(nc_kstgs);
                Say(kcs_kstgs);
                Say(ltcsH_kstgs);
                Say(ltcsM_kstgs);
                Say(ltcsS_kstgs);

                string encMes = "";
                for(int p = 0; p < message.Length; p++)
                {
                    encMes += (char)(message[p] ^ kcs);
                }
                Say(encMes);
                talk.Shutdown(SocketShutdown.Both);
            }
        }
    }
}
