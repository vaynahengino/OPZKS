﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Threading.Tasks;

namespace TGS
{
    class Program
    {

        public static Socket talk;
        public static int SecretLife = 120;
        public static int SeansLife = 60;
        public static int RazreshLife = 10;

        static void Say(int message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static int Listen()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            int tek;
            int.TryParse(message, out tek);
            return tek;
        }

        static void Initialize()
        {
            Random rnd = new Random();
            DateTime now = DateTime.Now;
            int seconds = DateTime.Now.Second + SecretLife;
            int minutes = now.Minute + seconds / 60;
            seconds %= 60;
            int hours = (now.Hour + minutes / 60) % 24;
            minutes %= 60;
            File.WriteAllText(@"D:\Laba_7\C_S_DB", "");
            File.WriteAllText(@"D:\Laba_7\S_TGS_DB", "");
            File.AppendAllText(@"D:\Laba_7\S_TGS_DB", "2 " + rnd.Next(1, 255).ToString() + " " + SecretLife.ToString() + " " + RazreshLife.ToString() + " " + hours.ToString() + " " + minutes.ToString() + " " + seconds.ToString() + Environment.NewLine);
            File.AppendAllText(@"D:\Laba_7\S_TGS_DB", "3 " + rnd.Next(1, 255).ToString() + " " + SecretLife.ToString() + " " + RazreshLife.ToString() + " " + hours.ToString() + " " + minutes.ToString() + " " + seconds.ToString());

        }

        static void Update()
        {
            while (true)
            {
                string[] db = File.ReadAllLines(@"D:\Laba_7\C_S_DB");
                File.WriteAllText(@"D:\Laba_7\C_S_DB", "");
                string[] newDB = new string[0];
                int id1 = -1;
                int id2 = -1;
                for (int p = 0; p < db.Length; p++)
                {

                    string tek = "";
                    int count = 0;
                    for (int q = 0; q < db[p].Length; q++)
                    {
                        if (db[p][q] == ' ') count++;
                        if (count == 5)
                        {
                            tek = db[p].Substring(q + 1);
                            break;
                        }
                    }
                    int hours;
                    int minutes;
                    int seconds;
                    int newId1;
                    int newId2;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out newId1);
                    int.TryParse(db[p].Substring(db[p].IndexOf(' ') + 1).Substring(0, db[p].IndexOf(' ')), out newId2);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out hours);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out minutes);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek, out seconds);
                    DateTime now = DateTime.Now;
                    int time1 = hours * 3600 + minutes * 60 + seconds;
                    int time2 = now.Hour * 3600 + now.Minute * 60 + now.Second;
                    if (Math.Abs(time2 - time1) > RazreshLife) time2 += 86400;
                    if (time1 < time2)
                    {
                        if (newId1 != id1 || newId2 != id2)
                        {
                            if (time1 + RazreshLife >= time2)
                            {
                                string[] old = newDB;
                                newDB = new string[old.Length + 1];
                                Array.Copy(old, newDB, old.Length);
                                newDB[old.Length] = db[p];
                            }
                        }
                        else if (newId1 == id1 && newId2 == id2 && time1 + RazreshLife >= time2)
                        {
                            string[] old = newDB;
                            newDB = new string[old.Length + 1];
                            Array.Copy(old, newDB, old.Length);
                            newDB[old.Length] = db[p];
                        }
                    }
                    else
                    {
                        string[] old = newDB;
                        newDB = new string[old.Length + 1];
                        Array.Copy(old, newDB, old.Length);
                        newDB[old.Length] = db[p];
                    }
                    id1 = newId1;
                    id2 = newId2;
                }
                File.WriteAllLines(@"D:\Laba_7\C_S_DB", newDB);

                db = File.ReadAllLines(@"D:\Laba_7\S_TGS_DB");
                File.WriteAllText(@"D:\Laba_7\S_TGS_DB", "");
                newDB = new string[0];
                Random rnd = new Random();
                int id = -1;
                for (int p = 0; p < db.Length; p++)
                {

                    string tek = "";
                    int count = 0;
                    for (int q = 0; q < db[p].Length; q++)
                    {
                        if (db[p][q] == ' ') count++;
                        if (count == 4)
                        {
                            tek = db[p].Substring(q + 1);
                            break;
                        }
                    }
                    int hours;
                    int minutes;
                    int seconds;
                    int newId;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out newId);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out hours);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out minutes);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek, out seconds);
                    DateTime now = DateTime.Now;
                    int time1 = hours * 3600 + minutes * 60 + seconds;
                    int time2 = now.Hour * 3600 + now.Minute * 60 + now.Second;
                    if (Math.Abs(time2 - time1) > SecretLife) time2 += 86400;
                    if (time1 < time2)
                    {
                        if (newId != id)
                        {
                            seconds = now.Second + SecretLife;
                            minutes = now.Minute + seconds / 60;
                            seconds %= 60;
                            hours = (now.Hour + minutes / 60) % 24;
                            minutes %= 60;
                            string[] old = newDB;
                            newDB = new string[old.Length + 1];
                            Array.Copy(old, newDB, old.Length);
                            newDB[old.Length] = newId + " " + rnd.Next(1, 255).ToString() + " " + SecretLife.ToString() + " " + RazreshLife.ToString() + " " + hours.ToString() + " " + minutes.ToString() + " " + seconds.ToString();
                            if (time1 + SecretLife >= time2)
                            {
                                old = newDB;
                                newDB = new string[old.Length + 1];
                                Array.Copy(old, newDB, old.Length);
                                newDB[old.Length] = db[p];
                            }
                        }
                        else if (newId == id && time1 + SecretLife >= time2)
                        {
                            string[] old = newDB;
                            newDB = new string[old.Length + 1];
                            Array.Copy(old, newDB, old.Length);
                            newDB[old.Length] = db[p];
                        }
                    }
                    else
                    {
                        string[] old = newDB;
                        newDB = new string[old.Length + 1];
                        Array.Copy(old, newDB, old.Length);
                        newDB[old.Length] = db[p];
                    }
                    id = newId;
                }
                File.WriteAllLines(@"D:\Laba_7\S_TGS_DB", newDB);
                Thread.Sleep(1000);
            }
        }

        static void Main(string[] args)
        {
            Initialize();
            Thread background = new Thread(Update);
            background.IsBackground = true;
            background.Start();
            IPAddress ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11111);
            Socket talking = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            talking.Bind(ipEndPoint);
            while (true)
            {
                Console.WriteLine("IP-Address: " + Dns.GetHostByName(Dns.GetHostName()).AddressList[0]);
                Console.WriteLine("Сервер выдачи разрешений. Жду подключения на порту 11111");
                Console.WriteLine();
                talking.Listen(1000);
                talk = talking.Accept();

                int idc_kctgs = Listen(); //Step 3
                int ids_kctgs = Listen();
                int ltctgsH_kctgs = Listen();
                int ltctgsM_kctgs = Listen();
                int ltctgsS_kctgs = Listen();
                int nc_kctgs = Listen();

                string[] db = File.ReadAllLines(@"D:\Laba_7\AS_TGS_DB"); // Step 4
                int kastgs = -1;
                int seconds = -1;
                int minutes = -1;
                int hours = -1;
                for (int p = 0; p < db.Length; p++)
                {
                    int tek;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out tek);
                    if (tek == 1)
                    {
                        db[p] = db[p].Substring(db[p].IndexOf(' ') + 1);
                        int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out tek);
                        if(tek == 0)
                        {
                            db[p] = db[p].Substring(db[p].IndexOf(' ') + 1);
                            int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out kastgs);
                            int count = 0;
                            for (int q = 0; q < db[p].Length; q++)
                            {
                                if (db[p][q] == ' ') count++;
                                if (count == 3)
                                {
                                    db[p] = db[p].Substring(q + 1);
                                    break;
                                }
                            }
                            int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out hours);
                            db[p] = db[p].Substring(db[p].IndexOf(' ') + 1);
                            int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out minutes);
                            db[p] = db[p].Substring(db[p].IndexOf(' ') + 1);
                            int.TryParse(db[p], out seconds);
                            break;
                        }                      
                    }
                }

                int idc = Listen() ^ kastgs;
                int idtgs = Listen() ^ kastgs;
                int kctgs = Listen() ^ kastgs;
                int ltctgsH = Listen() ^ kastgs;
                int ltctgsM = Listen() ^ kastgs;
                int ltctgsS = Listen() ^ kastgs;
                int nc = Listen() ^ kastgs;
                idc_kctgs ^= kctgs;
                ids_kctgs ^= kctgs;
                ltctgsH_kctgs ^= kctgs;
                ltctgsM_kctgs ^= kctgs;
                ltctgsS_kctgs ^= kctgs;
                nc_kctgs ^= kctgs;
                if (nc == nc_kctgs && ltctgsH == ltctgsH_kctgs && ltctgsM == ltctgsM_kctgs && ltctgsS == ltctgsS_kctgs)
                {
                    Console.WriteLine("Полученная от клиента " + idc + " информация верна");
                }
                else
                {
                    Console.WriteLine("Ошибка в полученной информации");
                }

                Random rnd = new Random();
                DateTime now = DateTime.Now;
                int kcs = rnd.Next(1, 255);
                db = File.ReadAllLines(@"D:\Laba_7\C_S_DB");
                string[] newDB2 = new string[db.Length + 1];
                Array.Copy(db, 0, newDB2, 1, db.Length);
                seconds = now.Second + RazreshLife;
                minutes = now.Minute + seconds / 60;
                seconds %= 60;
                hours = (now.Hour + minutes / 60) % 24;
                minutes %= 60;
                newDB2[0] = ids_kctgs + " " + idc + " " + kcs.ToString() + " " + SeansLife.ToString() + " " + RazreshLife.ToString() + " " + hours.ToString() + " " + minutes.ToString() + " " + seconds.ToString();
                File.WriteAllLines(@"D:\Laba_7\C_S_DB", newDB2);
                Console.WriteLine("Сгенерирован сеансовый ключ для разрешения C_S: " + kcs);
                

                Say(kcs ^ kctgs);
                Say(hours ^ kctgs); 
                Say(minutes ^ kctgs);
                Say(seconds ^ kctgs);

                db = File.ReadAllLines(@"D:\Laba_7\S_TGS_DB");
                int kstgs = -1;
                for (int p = 0; p < db.Length; p++)
                {
                    int tek;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out tek);
                    if (tek == ids_kctgs)
                    {
                        db[p] = db[p].Substring(db[p].IndexOf(' ') + 1);
                        int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out kstgs);
                        break;
                    }
                }
                Console.WriteLine("Текущий секретный ключ обнаружен: " + kstgs);
                Console.WriteLine();

                Say(nc ^ kstgs);
                Say(kcs ^ kstgs);
                Say(hours ^ kstgs);
                Say(minutes ^ kstgs);
                Say(seconds ^ kstgs);


            }
        }
    }
}
