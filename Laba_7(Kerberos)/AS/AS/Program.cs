﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Threading.Tasks;

namespace AS
{
    class Program
    {
        public static Socket talk;
        public static int SecretLife = 120;
        public static int SeansLife = 60;
        public static int RazreshLife = 10;


        static void Say(int message)
        {
            talk.Send(Encoding.UTF8.GetBytes(message.ToString()));
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
        }

        static int Listen()
        {
            int bytesRec;
            byte[] bytes = new byte[1024];
            bytesRec = talk.Receive(bytes);
            string message = Encoding.UTF8.GetString(bytes, 0, bytesRec);
            talk.Send(Encoding.UTF8.GetBytes("+"));
            int tek;
            int.TryParse(message, out tek);
            return tek;
        }

        static void Initialize()
        {
            Random rnd = new Random();
            DateTime now = DateTime.Now;
            int seconds = DateTime.Now.Second + SecretLife;
            int minutes = now.Minute + seconds / 60;
            seconds %= 60;
            int hours = (now.Hour + minutes / 60) % 24;
            minutes %= 60;
           
            File.WriteAllText(@"D:\Laba_7\AS_C_DB", "");
            File.AppendAllText(@"D:\Laba_7\AS_C_DB", "10 " + rnd.Next(1, 255).ToString() + " " + SecretLife.ToString() + " " + RazreshLife.ToString() + " " + hours.ToString() + " " + minutes.ToString() + " " + seconds.ToString());
            File.WriteAllText(@"D:\Laba_7\AS_TGS_DB", "");
            File.WriteAllText(@"D:\Laba_7\C_TGS_DB", "");
        }

        static void Update()
        {
            while(true)
            {
                string[] db = File.ReadAllLines(@"D:\Laba_7\AS_C_DB");
                File.WriteAllText(@"D:\Laba_7\AS_C_DB", "");
                string[] newDB = new string[0];
                Random rnd = new Random();
                int id = -1;
                for (int p = 0; p < db.Length; p++)
                {

                    string tek = "";
                    int count = 0;
                    for (int q = 0; q < db[p].Length; q++)
                    {
                        if (db[p][q] == ' ') count++;
                        if (count == 4)
                        {
                            tek = db[p].Substring(q + 1);
                            break;
                        }
                    }
                    int hours;
                    int minutes;
                    int seconds;
                    int newId;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out newId);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out hours);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out minutes);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek, out seconds);
                    DateTime now = DateTime.Now;
                    int time1 = hours * 3600 + minutes * 60 + seconds;
                    int time2 = now.Hour * 3600 + now.Minute * 60 + now.Second;
                    if (Math.Abs(time2 - time1) > SecretLife) time2 += 86400;
                    if (time1 < time2)
                    {
                        if (newId != id)
                        {
                            seconds = now.Second + SecretLife;
                            minutes = now.Minute + seconds / 60;
                            seconds %= 60;
                            hours = (now.Hour + minutes / 60) % 24;
                            minutes %= 60;
                            string[] old = newDB;
                            newDB = new string[old.Length + 1];
                            Array.Copy(old, newDB, old.Length);
                            newDB[old.Length] = newId + " " + rnd.Next(1, 255).ToString() + " " + SecretLife.ToString() + " " + RazreshLife.ToString() + " " + hours.ToString() + " " + minutes.ToString() + " " + seconds.ToString();
                            if (time1 + RazreshLife >= time2)
                            {
                                old = newDB;
                                newDB = new string[old.Length + 1];
                                Array.Copy(old, newDB, old.Length);
                                newDB[old.Length] = db[p];
                            }
                        }
                        else if(newId == id && time1 + RazreshLife >= time2)
                        {
                            string[] old = newDB;
                            newDB = new string[old.Length + 1];
                            Array.Copy(old, newDB, old.Length);
                            newDB[old.Length] = db[p];
                        }
                    }
                    else
                    {
                        string[] old = newDB;
                        newDB = new string[old.Length + 1];
                        Array.Copy(old, newDB, old.Length);
                        newDB[old.Length] = db[p];
                    }
                    id = newId;
                }
                File.WriteAllLines(@"D:\Laba_7\AS_C_DB", newDB);

                db = File.ReadAllLines(@"D:\Laba_7\AS_TGS_DB");
                File.WriteAllText(@"D:\Laba_7\AS_TGS_DB", "");
                newDB = new string[0];
                int id1 = -1;
                int id2 = -1;
                for (int p = 0; p < db.Length; p++)
                {

                    string tek = "";
                    int count = 0;
                    for (int q = 0; q < db[p].Length; q++)
                    {
                        if (db[p][q] == ' ') count++;
                        if (count == 5)
                        {
                            tek = db[p].Substring(q + 1);
                            break;
                        }
                    }
                    int hours;
                    int minutes;
                    int seconds;
                    int newId1;
                    int newId2;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out newId1);
                    int.TryParse(db[p].Substring(db[p].IndexOf(' ') + 1).Substring(0, db[p].IndexOf(' ')), out newId2);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out hours);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out minutes);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek, out seconds);
                    DateTime now = DateTime.Now;
                    int time1 = hours * 3600 + minutes * 60 + seconds;
                    int time2 = now.Hour * 3600 + now.Minute * 60 + now.Second;
                    if (Math.Abs(time2 - time1) > SeansLife) time2 += 86400;
                    if (time1 < time2)
                    {
                        if (newId1 != id1 || newId2 != id2)
                        {
                            if (time1 + RazreshLife >= time2)
                            {
                                string[] old = newDB;
                                newDB = new string[old.Length + 1];
                                Array.Copy(old, newDB, old.Length);
                                newDB[old.Length] = db[p];
                            }
                        }
                        else if (newId1 == id1 && newId2 == id2 && time1 + RazreshLife >= time2)
                        {
                            string[] old = newDB;
                            newDB = new string[old.Length + 1];
                            Array.Copy(old, newDB, old.Length);
                            newDB[old.Length] = db[p];
                        }
                    }
                    else
                    {
                        string[] old = newDB;
                        newDB = new string[old.Length + 1];
                        Array.Copy(old, newDB, old.Length);
                        newDB[old.Length] = db[p];
                    }
                    id1 = newId1;
                    id2 = newId2;
                }
                File.WriteAllLines(@"D:\Laba_7\AS_TGS_DB", newDB);


                db = File.ReadAllLines(@"D:\Laba_7\C_TGS_DB");
                File.WriteAllText(@"D:\Laba_7\C_TGS_DB", "");
                newDB = new string[0];
                id1 = -1;
                id2 = -1;
                for (int p = 0; p < db.Length; p++)
                {

                    string tek = "";
                    int count = 0;
                    for (int q = 0; q < db[p].Length; q++)
                    {
                        if (db[p][q] == ' ') count++;
                        if (count == 5)
                        {
                            tek = db[p].Substring(q + 1);
                            break;
                        }
                    }
                    int hours;
                    int minutes;
                    int seconds;
                    int newId1;
                    int newId2;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out newId1);
                    int.TryParse(db[p].Substring(db[p].IndexOf(' ') + 1).Substring(0, db[p].IndexOf(' ')), out newId2);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out hours);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek.Substring(0, tek.IndexOf(' ')), out minutes);
                    tek = tek.Substring(tek.IndexOf(' ') + 1);
                    int.TryParse(tek, out seconds);
                    DateTime now = DateTime.Now;
                    int time1 = hours * 3600 + minutes * 60 + seconds;
                    int time2 = now.Hour * 3600 + now.Minute * 60 + now.Second;
                    if (Math.Abs(time2 - time1) > SeansLife) time2 += 86400;
                    if (time1 < time2)
                    {
                        if (newId1 != id1 || newId2 != id2)
                        {
                            if (time1 + RazreshLife >= time2)
                            {
                                string[] old = newDB;
                                newDB = new string[old.Length + 1];
                                Array.Copy(old, newDB, old.Length);
                                newDB[old.Length] = db[p];
                            }
                        }
                        else if (newId1 == id1 && newId2 == id2 && time1 + RazreshLife >= time2)
                        {
                            string[] old = newDB;
                            newDB = new string[old.Length + 1];
                            Array.Copy(old, newDB, old.Length);
                            newDB[old.Length] = db[p];
                        }
                    }
                    else
                    {
                        string[] old = newDB;
                        newDB = new string[old.Length + 1];
                        Array.Copy(old, newDB, old.Length);
                        newDB[old.Length] = db[p];
                    }
                    id1 = newId1;
                    id2 = newId2;
                }
                File.WriteAllLines(@"D:\Laba_7\C_TGS_DB", newDB);

                Thread.Sleep(1000);
            }


        }

        static void Main(string[] args)
        {
            Initialize();
            Thread background = new Thread(Update);
            background.IsBackground = true;
            background.Start();
            IPAddress ipAddr = Dns.GetHostByName(Dns.GetHostName()).AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11110);
            Socket talking = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            talking.Bind(ipEndPoint);
            while (true)
            {
                Console.WriteLine("IP-Address: " + Dns.GetHostByName(Dns.GetHostName()).AddressList[0]);
                Console.WriteLine("Сервер аутентификации. Жду подключения на порту 11110");
                Console.WriteLine();
                
                
                talking.Listen(1000);
                talk = talking.Accept();
                
                int idc = Listen(); //Step 1
                int idtgs = Listen();
                int nc = Listen();
                Console.WriteLine("Обратился пользователь с ID " + idc); //Step 2
                string[] db = File.ReadAllLines(@"D:\Laba_7\AS_C_DB");
                int kcas = -1;
                for(int p = 0; p < db.Length; p++)
                {
                    int tek;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out tek);
                    if(tek == idc)
                    {
                        db[p] = db[p].Substring(db[p].IndexOf(' ') + 1);
                        int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out kcas);
                        break;
                    }
                }
                Console.WriteLine("Текущий секретный ключ обнаружен: " + kcas);
                db = File.ReadAllLines(@"D:\Laba_7\AS_TGS_DB");
                int kastgs = -1;
                int time1 = 0;
                for (int p = 0; p < db.Length; p++)
                {
                    int tek;
                    int.TryParse(db[p].Substring(0, db[p].IndexOf(' ')), out tek);
                    if (tek == idtgs)
                    {
                        string temp = db[p].Substring(db[p].IndexOf(' ') + 1);
                        int.TryParse(temp.Substring(0, temp.IndexOf(' ')), out kcas);
                        int count = 0;
                        for (int q = 0; q < temp.Length; q++)
                        {
                            if (temp[q] == ' ') count++;
                            if (count == 4)
                            {
                                temp = temp.Substring(q + 1);
                                break;
                            }
                        }
                        int hours1;
                        int minutes1;
                        int seconds1;
                        int.TryParse(temp.Substring(0, temp.IndexOf(' ')), out hours1);
                        temp = temp.Substring(temp.IndexOf(' ') + 1);
                        int.TryParse(temp.Substring(0, temp.IndexOf(' ')), out minutes1);
                        temp = temp.Substring(temp.IndexOf(' ') + 1);
                        int.TryParse(temp, out seconds1);
                        time1 = hours1 * 3600 + minutes1 * 60 + seconds1;
                        break;
                    }
                }
                DateTime now = DateTime.Now;
                int time2 = now.Hour * 3600 + now.Minute * 60 + now.Second;
                if (Math.Abs(time2 - time1) > SeansLife) time2 += 86400;
                if (kastgs == -1 || time1 < time2)
                {
                    
                    Random rand = new Random();
                    kastgs = rand.Next(1, 255);
                    Console.WriteLine("Сгенерирован новый сеансовый ключ AS_TGS: " + kastgs);
                    string[] newDB = new string[db.Length + 1];
                    Array.Copy(db, newDB, db.Length);
                    time1 = (time2 + SeansLife) % 86400;
                    int seconds1 = now.Second + SeansLife;
                    int minutes1 = now.Minute + seconds1 / 60;
                    seconds1 %= 60;
                    int hours1 = (now.Hour + minutes1 / 60) % 24;
                    minutes1 %= 60;
                    newDB[db.Length] = idtgs + " 0 " + kastgs.ToString() + " " + SeansLife.ToString() + " " + RazreshLife.ToString() + " " + hours1.ToString() + " " + minutes1.ToString() + " " + seconds1.ToString();
                    File.WriteAllLines(@"D:\Laba_7\AS_TGS_DB", newDB);
                }
                else
                {
                    Console.WriteLine("Найден сеансовый ключ AS_TGS: " + kastgs);
                }
                Random dop = new Random();
                Random rnd = new Random(dop.Next(1000));
                int kctgs = rnd.Next(1, 255);
                db = File.ReadAllLines(@"D:\Laba_7\C_TGS_DB");
                string[] newDB2 = new string[db.Length + 1];
                Array.Copy(db, 0, newDB2, 1, db.Length);
                int seconds = now.Second + SeansLife;
                int minutes = now.Minute + seconds / 60;
                seconds %= 60;
                int hours = (now.Hour + minutes / 60) % 24;
                minutes %= 60;
                newDB2[0] = idtgs + " " + idc + " " + kctgs.ToString() + " " + SeansLife.ToString() + " " + RazreshLife.ToString() + " " + hours.ToString() + " " + minutes.ToString() + " " + seconds.ToString();
                File.WriteAllLines(@"D:\Laba_7\C_TGS_DB", newDB2);
                Console.WriteLine("Сгенерирован сеансовый ключ C_TGS: " + kctgs);
                Console.WriteLine();

                Say(kctgs ^ kcas);
                Say(idtgs ^ kcas);
                Say(hours ^ kcas);
                Say(minutes ^ kcas);
                Say(seconds ^ kcas);

                Say(idc ^ kastgs);
                Say(idtgs ^ kastgs);
                Say(kctgs ^ kastgs);
                Say((time1 / 3600) ^ kastgs);
                Say((time1 % 3600 / 60) ^ kastgs);
                Say((time1 % 60) ^ kastgs);
                Say(nc ^ kastgs);
            }
        }
    }
}
