﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.IO;
using System.Security.Cryptography;

namespace Laba_4_RSA_
{
    class Program
    {
        private static BigInteger e = 65537;
        private static int MillerRabinCount = 1000;

        private static void writeKey()
        {
            Console.Clear();
            Console.Write("Введите имя файла открытого ключа: ");
            string m1 = Console.ReadLine();
            File.WriteAllText(m1, "");
            Console.Write("Введите имя файла закрытого ключа: ");
            string m2 = Console.ReadLine();
            File.WriteAllText(m2, "");
            BigInteger p;
            BigInteger q;
            while(true)
            {
                while(true)
                {
                    p = getRandom(32);
                    bool good = true;
                    for(int r = 2; r < 10000; r++)
                    {
                        if(p % r == 0)
                        {
                            good = false;
                            break;
                        }
                    }
                    if (good == false) continue;
                    if (PseudoPrime(p) && IsPrimeMillerRabin(p, MillerRabinCount, 16)) break;
                }
                Console.WriteLine("P сгенерировано");
                while (true)
                {
                    q = getRandom(32);
                    bool good = true;
                    for (int r = 2; r < 10000; r++)
                    {
                        if (q % r == 0)
                        {
                            good = false;
                            break;
                        }
                    }
                    if (good == false) continue;
                    if (PseudoPrime(p) && IsPrimeMillerRabin(q, MillerRabinCount, 16)) break;
                }
                BigInteger n = p * q;
                if (n.ToByteArray().Length != 64) continue;
                BigInteger eiler = (p - 1) * (q - 1);
                BigInteger d = Foo(e, eiler);
                AppendAllBytes(m1, n.ToByteArray());
                AppendAllBytes(m1, e.ToByteArray());
                AppendAllBytes(m2, n.ToByteArray());
                AppendAllBytes(m2, d.ToByteArray());
                break;
            }
            Console.Clear();
            Console.WriteLine("Ключи сохранены");
            Console.WriteLine();
        }

        static void Encode()
        {
            Console.Clear();
            Console.Write("Введите имя файла открытого ключа: ");
            string m1 = Console.ReadLine();
            if (!File.Exists(m1))
            {
                Console.WriteLine("Файл не существует");
                Console.WriteLine();
                return;
            }
            byte[] bytes = File.ReadAllBytes(m1);
            if(bytes.Length <= 64)
            {
                Console.WriteLine("Файл ключей изменён или повреждён");
                Console.WriteLine();
                return;
            }
            byte[] temp = new byte[64];
            Array.Copy(bytes, temp, 64);
            BigInteger n = new BigInteger(temp);
            temp = new byte[bytes.Length - 64];
            Array.Copy(bytes, 64, temp, 0, bytes.Length - 64);
            BigInteger e = new BigInteger(temp);
            Console.Write("Введите имя исходного файла: ");
            m1 = Console.ReadLine();
            if (!File.Exists(m1))
            {
                Console.WriteLine("Файл не существует");
                Console.WriteLine();
                return;
            }
            Console.Write("Введите имя итогового файла: ");
            string m2 = Console.ReadLine();
            File.WriteAllText(m2, "");

            bytes = File.ReadAllBytes(m1);
            string message = "";
            for (int p = 0; p < bytes.Length; p++)
            {
                message += (char)bytes[p];
            }

            
            byte[] hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(message));
            AppendAllBytes(m2, hash);
            hash = new byte[4];
            hash[3] = (byte)(message.Length / 16777216);
            hash[2] = (byte)(message.Length % 16777216 / 65536);
            hash[1] = (byte)(message.Length % 65536 / 256);
            hash[0] = (byte)(message.Length % 256);
            AppendAllBytes(m2, hash);
            if (message.Length == 0)
            {
                message += " ";
            }
            while (message.Length % 63 != 0)
            {
                message += " ";
            }
            for(int p = 0; p < message.Length; p += 63)
            {
                temp = new byte[63];
                for(int q = 0; q < 63; q++)
                {
                    temp[q] = (byte)message[p + q];
                }
                hash = new byte[64];
                Array.Copy(temp, hash, temp.Length);
                hash[63] = 0;
                BigInteger tek = new BigInteger(hash);
                tek = BigInteger.ModPow(tek, e, n);
                temp = tek.ToByteArray();
                hash = new byte[65];
                for (int q = 0; q < 65; q++)
                {
                    hash[q] = 0;
                }
                Array.Copy(temp, 0, hash, 0, temp.Length);
                AppendAllBytes(m2, hash);
            }
            Console.Clear();
            Console.WriteLine("Файл успешно зашифрован");
            Console.WriteLine();
        }

        static void Decode()
        {
            Console.Clear();
            Console.Write("Введите имя файла закрытого ключа: ");
            string m1 = Console.ReadLine();
            if (!File.Exists(m1))
            {
                Console.WriteLine("Файл не существует");
                Console.WriteLine();
                return;
            }
            byte[] bytes = File.ReadAllBytes(m1);
            if (bytes.Length <= 64)
            {
                Console.WriteLine("Файл ключей изменён или повреждён");
                Console.WriteLine();
                return;
            }
            byte[] temp = new byte[64];
            Array.Copy(bytes, temp, 64);
            BigInteger n = new BigInteger(temp);
            temp = new byte[bytes.Length - 64];
            Array.Copy(bytes, 64, temp, 0, bytes.Length - 64);
            BigInteger d = new BigInteger(temp);
            Console.Write("Введите имя исходного файла: ");
            m1 = Console.ReadLine();
            if (!File.Exists(m1))
            {
                Console.WriteLine("Файл не существует");
                Console.WriteLine();
                return;
            }
            Console.Write("Введите имя итогового файла: ");
            string m2 = Console.ReadLine();
            File.WriteAllText(m2, "");

            bytes = File.ReadAllBytes(m1);
            string message = "";
            for (int p = 0; p < bytes.Length; p++)
            {
                message += (char)bytes[p];
            }
            if(message.Length <= 20)
            {
                Console.WriteLine("Неправильная длинна зашифрованного файла");
                Console.WriteLine();
                return;
            }
            byte[] ishHash = new byte[16];
            byte[] hash;
            Array.Copy(bytes, ishHash, 16);
            hash = new byte[5];
            Array.Copy(bytes, 16, hash, 0, 4);
            hash[4] = 0;
            BigInteger length = new BigInteger(hash);

            message = message.Substring(20);
            if(message.Length % 65 != 0)
            {
                Console.WriteLine("Неправильная длинна зашифрованного файла");
                Console.WriteLine();
                return;
            }
            for (int p = 20; p < bytes.Length; p += 65)
            {
                temp = new byte[65];
                for (int q = 0; q < 65; q++)
                {
                    temp[q] = bytes[p + q];
                }

                hash = new byte[66];
                Array.Copy(temp, hash, temp.Length);
                hash[65] = 0;
                BigInteger tek = new BigInteger(hash);
                tek = BigInteger.ModPow(tek, d, n);
                temp = tek.ToByteArray();
                hash = new byte[63];
                for (int q = 0; q < 63; q++)
                {
                    hash[q] = 0;
                }
                for(int q = 0; q < temp.Length;q++)
                {
                    if (q == 63) break;
                    hash[q] = temp[q];
                }
                if(p + 65 >= bytes.Length)
                {
                    int count = 0;
                    while(length % 63 != 0)
                    {
                        length++;
                        count++;
                    }
                    temp = new byte[63 - count];
                    Console.WriteLine(count);
                    for(int q = 0; q< temp.Length;q++)
                    {
                        temp[q] = hash[q];
                    }
                    hash = new byte[temp.Length];
                    Array.Copy(temp, hash, temp.Length);
                }
                AppendAllBytes(m2, hash);
            }
            bytes = File.ReadAllBytes(m2);
            message = "";
            for (int p = 0; p < bytes.Length; p++)
            {
                message += (char)bytes[p];
            }
            byte[] tekHash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(message));
            bool good = true;
            for(int p = 0; p < 16; p++)
            {
                if(ishHash[p] != tekHash[p])
                {
                    good = false;
                    break;
                }
            }
            if(!good)
            {
                Console.Clear();
                Console.WriteLine("Файл расшифрован с ошибками");
                Console.WriteLine();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Файл успешно расшифрован");
                Console.WriteLine();
            }
            
        }


        static BigInteger getRandom(int length)
        {
            byte[] data = new byte[length];
            while (true)
            {
                Random random = new Random((int)DateTime.Now.Ticks);
                random.NextBytes(data);
                if (new BigInteger(data) > 1) break;
            }
            return new BigInteger(data);
        }

        private static bool Witness(BigInteger a, BigInteger n)
        {

            BigInteger d = n - 1;
            int s = 0;
            while (d % 2 == 0)
            {
                s++;
                d /= 2;
            }

            if (BigInteger.ModPow(a, d, n) == 1) return true;

            for (int p = 0; p <= s - 1; p++)
            {
                if (BigInteger.ModPow(a, BigInteger.Pow(2, p) * d, n) == n - 1) return true;
            }
            return false;
        }

        public static bool IsPrimeMillerRabin(BigInteger n, int s, int length)
        {
            if (n % 2 == 0 || n <= 1) return false;
            for (int j = 0; j < s; j++)
            {
                BigInteger a = 0;
                while (true)
                {
                    a = getRandom(length);
                    if (a < n) break;
                }
                if (!Witness(a, n))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool PseudoPrime(BigInteger n)
        {
            BigInteger modularExponentiation = ModularExponentiation(2,BigInteger.Subtract(n, 1), n);
            if (!modularExponentiation.IsOne)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static BigInteger ModularExponentiation(BigInteger a, BigInteger b, BigInteger n)
        {
            return BigInteger.ModPow(a, b, n);
        }

        private static BigInteger GCD(BigInteger a, BigInteger b, out BigInteger x, out BigInteger y)
        {
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }
            BigInteger x1, y1;
            BigInteger d = GCD(b % a, a, out x1, out y1);
            x = y1 - (b / a) * x1;
            y = x1;
            return d;
        }
        private static BigInteger Foo(BigInteger a, BigInteger m)
        {
            BigInteger x, y;
            BigInteger g = GCD(a, m, out x, out y);
            if (g != 1)
                throw new ArgumentException();
            return (x % m + m) % m;
        }

        public static void AppendAllBytes(string path, byte[] bytes)
        {
            using (var stream = new FileStream(path, FileMode.Append))
            {
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Программа шифрования и дешифрования RSA");
                Console.WriteLine("Предлагаемые действия:");
                Console.WriteLine();
                Console.WriteLine("1) Сгенерировать ключ");
                Console.WriteLine("2) Зашифровать файл");
                Console.WriteLine("3) Расшифровать файл");
                Console.WriteLine("4) Выйти из программы");
                Console.WriteLine();
                Console.Write("Ваше действие: ");
                int choice = 4;
                int.TryParse(Console.ReadLine(), out choice);

                if (choice == 1)
                {
                    writeKey();
                }
                else if (choice == 2)
                {
                    Encode();
                }
                else if (choice == 3)
                {
                    Decode();
                }
                else
                {
                    break;
                }
            }
        }
    }
}
